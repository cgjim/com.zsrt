/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50546
Source Host           : localhost:3306
Source Database       : common-mapper-demos

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2016-12-16 00:43:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bbu_user_t
-- ----------------------------
DROP TABLE IF EXISTS `bbu_user_t`;
CREATE TABLE `bbu_user_t` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(64) DEFAULT NULL,
  `USER_AGE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbu_user_t
-- ----------------------------
INSERT INTO `bbu_user_t` VALUES ('4', 'scott2', '30');
