项目访问路径

增加： http://localhost:8080/ssm-mapper/user/add?userName=scott&userAge=33
删除： http://localhost:8080/ssm-mapper/user/delete?id=2
修改： http://localhost:8080/ssm-mapper/user/update?id=4&userName=scott2&userAge=30
查询： http://localhost:8080/ssm-mapper/user/findById?id=4


1:本项目演示了在ssm框架中加入通用mapper的支持 主要分为如下3步

1.1	导包
<!-- 通用mapper的配置 -->
<dependency>
	<groupId>tk.mybatis</groupId>
	<artifactId>mapper</artifactId>
	<version>3.3.8</version>
</dependency>
<!-- 通用mapper的配置结束 -->


1.2 在applicationContext.xml文件中配置mapper的地方加上下面这句话
<!-- ------------------------------------------------myBatis的配置 开始 --------------------------------------------->
<!-- myBatis和Spring的整合 01 配置xml文件所在的目录 开始 -->
<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
	<property name="dataSource" ref="dataSource" />
	<!-- 自动扫描entity目录, 省掉Configuration.xml里的手工配置 -->
	<!-- 只要在com.szrt.ssm这个包及其子包下面所有以xml结尾的文件都接受mybatis的管理 -->
	<!-- <property name="mapperLocations" value="classpath*:com/szrt/ssm/procedure/**/*.xml" 
		/> -->
	<property name="mapperLocations" value="classpath*:com/**/*.xml" />

	<!-- 通用Mapper配置2.1开始 -->
	<property name="typeAliasesPackage" value="com.isea533.mybatis.model" />
	<!-- 通用Mapper配置2.1结束 -->
</bean>
<!-- myBatis和Spring的整合 01 配置xml文件所在的目录 结束 -->

<!-- 通用Mapper配置2.2开始 -->
<bean class="tk.mybatis.spring.mapper.MapperScannerConfigurer">
	<!-- 2.2.1 basePackage配置系统中所有的dao文件所在的包名  -->
	<property name="basePackage" value="com.szrt.ssm.mapper.bbu.*.dao,com.isea533.mybatis.mapper" />
	<!-- 2.2.2 markerInterface配置系统总通过dao的名字 这里不需要加泛型 -->
	<property name="markerInterface" value="com.szrt.ssm.mapper.bbu.base.BaseDao" />
</bean>
<!-- 通用Mapper配置2.2结束 -->


<!-- myBatis和Spring的整合 02 配置dao文件所在的目录 接口绑定 开始 -->
<bean id="mybatisDaoConfig" class="org.mybatis.spring.mapper.MapperScannerConfigurer">
	<property name="basePackage" value="com.**.dao" />
	<property name="sqlSessionFactoryBeanName" value="sqlSessionFactory" />
</bean>
<!-- myBatis和Spring的整合 02 配置dao文件所在的目录 接口绑定 结束 -->
<!-- ------------------------------------------------myBatis的配置 开始 --------------------------------------------->


1.3 在对应的实体类上加上注解 而且要在主键上加上注解
@Scope("prototype")
@Table(name="bbu_user_t")
public class UserEntity{

	@Id
	private Integer id;
	
}


参考手册：http://git.oschina.net/free/Mapper/blob/master/wiki/mapper3/5.Mappers.md

	   



