package com.szrt.ssm.mapper.bbu.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.szrt.ssm.mapper.bbu.user.dao.UserDao;
import com.szrt.ssm.mapper.bbu.user.entity.UserEntity;


@Service
public class UserService {
	
	@Autowired
	private UserDao dao;
	
	@Transactional
	public void add(UserEntity entity, Map<String, Object> result) {
		try{
			int counter =dao.insert(entity);
			System.out.println("counter="+counter);
			result.put("flag", "1");
			result.put("msg", "插入成功...");
			result.put("entity", entity);
		}catch(Exception e){
			result.put("flag", "0");
			result.put("msg", "插入失败...");
			e.printStackTrace();
		}
		
	}
	
	@Transactional
	public void delete(Integer id, Map<String, Object> result) {
		try{
			int counter =dao.deleteByPrimaryKey(id);
			System.out.println("counter="+counter);
			result.put("flag", "1");
			result.put("msg", "删除成功...");
		}catch(Exception e){
			result.put("flag", "0");
			result.put("msg", "删除失败...");
			e.printStackTrace();
		}
		
	}
	@Transactional
	public void update(UserEntity entity, Map<String, Object> result) {
		try{
			int counter =dao.updateByPrimaryKeySelective(entity);
			System.out.println("counter="+counter);
			result.put("flag", "1");
			result.put("msg", " 修改成功...");
		}catch(Exception e){
			result.put("flag", "0");
			result.put("msg", "修改失败...");
			e.printStackTrace();
		}
		
	}

	@Transactional(readOnly=true)
	public void findById(Integer id, Map<String, Object> result) {
		try{
			UserEntity entity =dao.selectByPrimaryKey(id);
			System.out.println("entity="+entity);
			result.put("entity", entity);
			result.put("flag", "1");
			result.put("msg", "查询成功...");
		}catch(Exception e){
			result.put("flag", "0");
			result.put("msg", "查询失败...");
			e.printStackTrace();
		}
		
	}
	
	
	@Transactional(readOnly=true)
	public void fenye(Map<String,Integer> fy,Map<String, Object> result){
		try{
			List<UserEntity> list =dao.fenye(fy);
			result.put("list", list);
			result.put("flag", "1");
			result.put("msg", "查询成功...");
		}catch(Exception e){
			result.put("flag", "0");
			result.put("msg", "查询失败...");
			e.printStackTrace();
		}
		
	}
}
