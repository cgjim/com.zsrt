package com.szrt.ssm.mapper.bbu.user.entity;

import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;

@Scope("prototype")
@Table(name="bbu_user_t")
public class UserEntity {
	
	@Id
	private Integer id;
	
	private String userName;
	
	private Integer userAge;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUserAge() {
		return userAge;
	}

	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", userName=" + userName + ", userAge=" + userAge + "]";
	}
	
}
