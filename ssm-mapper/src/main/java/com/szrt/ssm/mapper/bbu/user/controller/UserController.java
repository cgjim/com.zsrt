package com.szrt.ssm.mapper.bbu.user.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szrt.ssm.mapper.bbu.user.entity.UserEntity;
import com.szrt.ssm.mapper.bbu.user.service.UserService;

@Controller
@RequestMapping(value="/user")
public class UserController {
	
	@Autowired
	private UserService service;
	
	
	// http://localhost:8080/ssm-mapper/user/add?userName=scott&userAge=33
	@RequestMapping(value="/add")
	@ResponseBody
	public Map<String,Object> add(UserEntity entity){
		Map<String,Object> result = new HashMap<String,Object>();
		service.add(entity, result);
		return result;
	}
	
	// http://localhost:8080/ssm-mapper/user/delete?id=2
	@RequestMapping(value="/delete")
	@ResponseBody
	public Map<String,Object> delete(Integer id){
		Map<String,Object> result = new HashMap<String,Object>();
		service.delete(id, result);
		return result;
	}
	
	//http://localhost:8080/ssm-mapper/user/update?id=4&userName=scott2&userAge=30
	@RequestMapping(value="/update")
	@ResponseBody
	public Map<String,Object> update(UserEntity entity){
		Map<String,Object> result = new HashMap<String,Object>();
		service.update(entity, result);
		return result;
	}
	
	//http://localhost:8080/ssm-mapper/user/findById?id=4
	@RequestMapping(value="/findById")
	@ResponseBody
	public Map<String,Object> findById(Integer id){
		Map<String,Object> result = new HashMap<String,Object>();
		service.findById(id, result);
		return result;
	}
	//http://localhost:8080/ssm-mapper/user/fenye?before=0&after=2
	@RequestMapping(value="/fenye")
	@ResponseBody
	public Map<String,Object> fenye(Integer before,Integer after){
		Map<String,Integer> fy = new HashMap<String,Integer>();
		fy.put("before", before);
		fy.put("after", after);
		Map<String,Object> result = new HashMap<String,Object>();
		service.fenye(fy, result);
		return result;
	}

}
