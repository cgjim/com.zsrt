package com.szrt.ssm.mapper.bbu.user.dao;

import java.util.List;
import java.util.Map;

import com.szrt.ssm.mapper.bbu.base.BaseDao;
import com.szrt.ssm.mapper.bbu.user.entity.UserEntity;

public interface UserDao extends BaseDao<UserEntity> {
	
	
	/**
	 * 分页
	 * @param map
	 * @return
	 */
	List<UserEntity> fenye(Map<String,Integer> map);

}
