package com.szrt.ssm.mapper.bbu.base;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface BaseDao<Entity> extends Mapper<Entity>,MySqlMapper<Entity> {

}
