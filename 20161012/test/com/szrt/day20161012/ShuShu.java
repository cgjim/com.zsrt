package com.szrt.day20161012;

import java.util.Scanner;

import org.junit.Test;

public class ShuShu {
	/*从键盘输入一个整数，判断是否是水仙花数   3位 153
	*/
	@Test
	public void test() {
		int num2,ge,shi,bai;
		System.out.println("亲，请输入3个数字");
		Scanner kb= new Scanner(System.in);
		num2 = kb.nextInt();
		bai=num2/100%10;
		shi=num2/10%10;
		ge=num2%10;
		
		if(ge*ge*ge+shi*shi*shi+bai*bai*bai == num2){
		System.out.println("这个是水仙花数");
		}else{
			System.out.println("不是水仙花树");
		}
		
				
		
	}
	@Test//计算回文数
	public void test2() {
		int num,wan,qian,shi,ge;
		System.out.println("亲，请输入5个数字");
		Scanner sn=new Scanner(System.in);
		num=sn.nextInt();
		wan = num/10000;
		qian = num/1000%10;
		shi=num/10%10;
		ge=num%10;
		if(wan==ge&&qian==shi){
			System.out.println(num+"是回文数！");
		}
		else {
			System.out.println("error");
		}
	}
	
	@Test//计算回文数
	public void test3() {
		System.out.println("请输入一个数：");
		Scanner sc = new Scanner(System.in);
		String number = sc.next();
		boolean flag = true;
		for(int i=0;i<number.trim().length()/2;i++){
		    if(!String.valueOf(number.charAt(i)).equals(String.valueOf(number.charAt(number.trim().length()-i-1)))){
		    flag = false;
		    break;
		    }
		}
		if(flag){
			System.out.println("它是回文数");
		}else{
		    System.out.println("它不是回文数");
		}

	}
	@Test//计算回文数
	public void test4() {
		System.out.println("请输入一个数：");
		Scanner sc = new Scanner(System.in);
		String number = sc.next();
		sc.close();
		StringBuffer sb=new StringBuffer(number);
		String fan=sb.reverse().toString();
		if(fan.equals(number)){
			System.out.println("它是回文数");
		}else{
			System.out.println("它不是回文数");
		}
	}
}
