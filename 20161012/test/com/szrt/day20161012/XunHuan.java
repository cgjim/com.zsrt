package com.szrt.day20161012;

import static org.junit.Assert.*;

import org.junit.Test;

public class XunHuan {
//累加10
	@Test
	public void test() {
		int i=1;
		int sum=0;
		while(i<=10){
			sum=sum+i;
			i=i+1;
			System.out.println(sum+"\t"+i);
		}
		System.out.println(sum);
	}
/**
 * 从1到100以内的所有的奇数相加
从1到100以内的所有的能被3整除的数字相加
 */
	@Test
	public void test2() {
		int i=1;
		int sum=0;
		while(i<101){
			if(i%2!=0){
				sum=sum+i;
		}
			i=i+1;
	}
		System.out.println(sum);
	}
	//从1到100以内的所有的能被3整除的数字相加
	@Test
	public void test3() {
		int i=1;
		int sum=0;
		while(i<101){
			if(i%3==0){
				sum=sum+i;
		}
			i=i+1;
	}
		System.out.println(sum);
	}
	@Test
	//从1到10以内的所有的能被2整除的数字相乘
	public void test4() {
		int sum=1;
		for(int i=1;i<11;i++){
			if(i%2==0){
				sum=sum*i;
			}
		}
		System.out.println(sum);
	}
	@Test
	public void test5() {
		
		for(int i=0;i<10;i++){
			for(int j=1;j<i;j++){
				System.out.print("*\t");
			}
			System.out.println("");
		}
	}
	//99乘法表
	@Test
	public void test6() {
		int sum=1;
		for(int i=1;i<=9;i++){
			for(int j=1;j<=i;j++){
				sum=i*j;
				System.out.print(i+"*"+j+"="+sum+"\t");
			}
			System.out.println();
		}
	}	
}
