package com.szrt.day20161012;

import java.util.Scanner;

import org.junit.Test;

/**
 * 
 * 专门用来测试
 *
 */
public class IfTest {
	@Test
	public void if01(){
		System.out.println("hello world!");
		Scanner sc=new Scanner(System.in);
		int grade=sc.nextInt();
		if(grade==100){
			System.out.println("送车");
		}else if(grade>=90){
			System.out.println("MP4");
		}else if(grade>=60){
			System.out.println("买书");
		}else{
			System.out.println("不买");
		}
	}
	@Test
	public void if02(){
		System.out.println("有需要请按1,2,3,4");
		Scanner sc=new Scanner(System.in);
		int age=sc.nextInt();
		switch(age){
			case 1:System.out.println("爸爸");
			break;
			case 2:System.out.println("妈妈");
			break;
			case 3:System.out.println("爷爷");
			break;
			case 4:System.out.println("奶奶");
			break;
			
		}
	}
	
}
