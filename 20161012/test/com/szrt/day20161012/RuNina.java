package com.szrt.day20161012;

import java.util.Scanner;

import org.junit.Test;

public class RuNina {
	/**
	 * 普通年能被4整除且不能被100整除的为闰年.（如2004年就是闰年,1901年不是闰年）
	 * ②、世纪年能被400整除的是闰年.(如2000年是闰年,1900年不是闰年)
	 * ③、对于数值很大的年份,这年如果能整除3200,并且能整除172800则是闰年.如172800年是闰年,
	 * 86400年不是闰年(因为虽然能整除3200,但不能整除172800)（此按一回归年365天5h48'45.5''计算）.
	 * 
	 */
	@Test
	public void if01(){
		System.out.println("请你输入年份：");
		Scanner sc=new Scanner(System.in);
		int a=sc.nextInt();	
		if(a%4==0&&a%100!=0){
			System.out.println(a+"是平年的闰年");
		}else if(a%400==00){
			System.out.println(a+"是世纪年的闰年");
		}else{
			System.out.println("不是闰年！");
		}
	}
	
}
