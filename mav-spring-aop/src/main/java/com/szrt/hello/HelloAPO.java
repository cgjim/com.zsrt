package com.szrt.hello;

/**
 * 第一个切面方程 
 * 1 配置文件xml
 * 2 一个接口 两方法 hello 
 * @author chenguangjin
 *
 */
public interface HelloAPO {

	void hello();
	void doPrint();
}
