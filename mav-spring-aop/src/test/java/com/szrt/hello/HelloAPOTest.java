package com.szrt.hello;


import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloAPOTest {

	@Test
	public void test() {
		 ApplicationContext ctx =   new ClassPathXmlApplicationContext("aop.xml");
		 HelloAPO he = (HelloAPO) ctx.getBean("HelloAPO01Impl");
		 HelloAPO he2 = (HelloAPO) ctx.getBean("HelloAPO02Impl");
		 he.hello();
		 he.doPrint();
	}

}
