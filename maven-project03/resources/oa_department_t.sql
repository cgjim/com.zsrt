/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.6.0-m4 : Database - db1610
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`db1610` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db1610`;

/*Table structure for table `oa_department_t` */

DROP TABLE IF EXISTS `oa_department_t`;

CREATE TABLE `oa_department_t` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `LOCATION` varchar(200) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `oa_department_t` */

insert  into `oa_department_t`(`ID`,`NAME`,`LOCATION`) values (3,'BB','深圳');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
