package com.szrt.banji09.maven_project03.homework.day1027.oa.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.szrt.banji09.maven_project03.day1027.constans.JDBCConstans;

public class JDBCUtil {
	
	public static Connection ConnectionDB() throws Exception{
		Class.forName(PropertiesUtil.findValueByKey(JDBCConstans.DRIVER));
		//String url="jdbc:mysql://127.0.0.1:3306/db1610";
		String url=PropertiesUtil.findValueByKey(JDBCConstans.URL);
		String user=PropertiesUtil.findValueByKey(JDBCConstans.USER);
		String password=PropertiesUtil.findValueByKey(JDBCConstans.PASSWORD);
		Connection connection=DriverManager.getConnection(url, user,password);
		return connection;
	}
	
	/**
	 * 数据库连接的关闭和语句关闭
	 * @throws SQLException 
	 */
	public static void connectionCloseAndStatementColse(PreparedStatement ps,Connection connection) throws SQLException{
		ps.close();
		connection.close();
	}
}
