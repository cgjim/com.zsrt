package com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao.impl;

import java.sql.Connection;

import com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao.BaseDao;
import com.szrt.banji09.maven_project03.homework.day1027.oa.util.JDBCUtil;

/**
 *  1 : 父亲知道怎么去连接数据库...
 * 2:  父亲不知道该往哪个表里面插入数据 ??  
 * @author Administrator
 *
 * @param <T>
 */
public abstract class BaseDaoImpl<T> implements BaseDao<T>{
	private static Connection connection=null;
	static{
		try {
			connection=JDBCUtil.ConnectionDB();
			System.out.println(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection() {
		return connection;
	}
	public int delete(int id) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

}
