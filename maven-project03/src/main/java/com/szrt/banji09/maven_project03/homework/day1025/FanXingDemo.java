package com.szrt.banji09.maven_project03.homework.day1025;

import com.google.gson.Gson;

/**
 * 	TeacherEntity   id  name  phone  salary birthday
	黄老师
	王老师
	模仿  xfTest03  搞1个泛型的list  用 json打印
 * @author Administrator
 *
 */
public class FanXingDemo<T> {
	
	public static<T> String toJson(T x){
		Gson json = new Gson();
		return json.toJson(x);
	}
}
