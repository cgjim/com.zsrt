package com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao;

public interface BaseDao<T> {

	int add(T entity) throws Exception;
	
	int delete(int id) throws Exception;
	/**
	 * 查询数据表
	 * @throws Exception
	 */
	void select()throws Exception;
	
	/**
	 * 通过id修改部门名称
	 */
	int alter(T entity) throws Exception;
	
	int alter2(String name,int id ) throws Exception;
}
