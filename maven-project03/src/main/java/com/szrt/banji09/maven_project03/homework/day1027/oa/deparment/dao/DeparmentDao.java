package com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.dao;

import com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao.BaseDao;
import com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.entity.DeparmentEntity;

public interface DeparmentDao extends BaseDao<DeparmentEntity> {
	
}
