package com.szrt.banji09.maven_project03.homework.day1025;
/**
 * id  name  phone  salary birthday
 * @author Administrator
 *
 */
public class StudentEntity {
	private int id;
	private String name;
	private double salary ;
	private String birthday;
	
	public StudentEntity(int id, String name, double salary, String birthday) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.birthday = birthday;
	}
	
	public StudentEntity(){
		
	}
	
}
