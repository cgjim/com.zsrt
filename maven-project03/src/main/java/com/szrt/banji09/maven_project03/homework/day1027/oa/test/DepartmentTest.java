package com.szrt.banji09.maven_project03.homework.day1027.oa.test;
import org.junit.Test;

import com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao.BaseDao;

import com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.dao.impl.DeparmentDaoImpl;
import com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.entity.DeparmentEntity;

public class DepartmentTest {
	private BaseDao<DeparmentEntity> dao=new DeparmentDaoImpl();
	@Test
	public void add() throws Exception {
		DeparmentEntity de=new DeparmentEntity(42, "DD", "故园");
		dao.add(de);
	}
	
	@Test
	public void delete() throws Exception {
		dao.delete(2);
	}
	@Test
	public void select() throws Exception {
		dao.select();
	}
	
	/**
	 * 修改  
	 * 新建一个构造方法(String dName,Integer dId)  符合需求
	 * 通过id修改部门名称
	 * @throws Exception
	 */
	@Test
	public void alter() throws Exception {
	DeparmentEntity de=new DeparmentEntity("大大部", 3);
	dao.alter(de);
	}
	
	@Test
	public void alter2() throws Exception {
		dao.alter2("嘿嘿部", 11);
	}
}
