package com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.google.gson.Gson;
import com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao.impl.BaseDaoImpl;
import com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.dao.DeparmentDao;
import com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.entity.DeparmentEntity;
import com.szrt.banji09.maven_project03.homework.day1027.oa.util.JDBCUtil;

/**
 * 具体实现类写 具体增删方法
 * @author Administrator
 *ID      NAME    LOCATION
 */
public class DeparmentDaoImpl extends BaseDaoImpl<DeparmentEntity> implements DeparmentDao{
	/**
	 * 增加
	 */
	@Override
	public int add(DeparmentEntity entity) throws Exception {
		Connection connection=getConnection(); 
		String sql ="insert into oa_department_t() values(?,?,?)";
		System.out.println(sql);
		PreparedStatement ps=connection.prepareStatement(sql);
		int id=entity.getdId();
		String name=entity.getdName();
		String location=entity.getdLocation();
		//设value的值 下标为第几个~~1 2 3
		ps.setInt(1, id);
		ps.setString(2, name);
		ps.setString(3, location);
		int counter=ps.executeUpdate();
		System.out.println(counter);
		System.out.println("操作成功...");
		
		//关闭  先关语句，再关连接
		JDBCUtil.connectionCloseAndStatementColse(ps, connection);
		return counter;
	}

	@Override
	public int delete(int id) throws Exception {
		Connection connection=getConnection(); 
		String sql ="DELETE FROM oa_department_t WHERE ID=?";
		System.out.println(sql);
		PreparedStatement ps=connection.prepareStatement(sql);
		//设置语句 ？
		ps.setInt(1, id);
		
		int counter=ps.executeUpdate();
		System.out.println(counter);
		System.out.println("操作成功...");
		//关闭  先关语句，再关连接
		JDBCUtil.connectionCloseAndStatementColse(ps, connection);
		return counter;
	}

	/**
	 * 查询
	 */
	@Override
	public void select() throws Exception {
		Connection connection=getConnection(); 
		String sql ="SELECT * FROM oa_department_t";
		
		PreparedStatement ps=connection.prepareStatement(sql);
		//有个结果集 接收
		ResultSet rs=ps.executeQuery();
		while(rs.next()){
			int id=rs.getInt("ID");
			String name=rs.getString("NAME");
			String lo=rs.getString("LOCATION");
			DeparmentEntity de=new DeparmentEntity(id, name, lo);
			//用Gson的toJson输出 将地址改为结果
			//循环一次输出一次
			Gson json=new Gson();
			System.out.println(json.toJson(de));
		}
		JDBCUtil.connectionCloseAndStatementColse(ps, connection);
	}
/**UPDATE oa_department_t SET name='大神部' WHERE ID=1
 * 修改
 */

@Override
public int alter(DeparmentEntity entity) throws Exception {
	Connection connection=getConnection(); 
	String sql ="UPDATE oa_department_t SET name=? WHERE ID=?";
	System.out.println(sql);
	PreparedStatement ps=connection.prepareStatement(sql);
	int id=entity.getdId();
	String name=entity.getdName();
	//设value的值 下标为第几个~~1 2 3
	ps.setString(1, name);
	ps.setInt(2, id);
	int counter=ps.executeUpdate();
	System.out.println(counter);
	System.out.println("操作成功...");
	
	//关闭  先关语句，再关连接
	JDBCUtil.connectionCloseAndStatementColse(ps, connection);
	return counter;
}

@Override
public int alter2(String name, int id) throws Exception {
	Connection connection=getConnection(); 
	String sql ="UPDATE oa_department_t SET name=? WHERE ID=?";
	System.out.println(sql);
	PreparedStatement ps=connection.prepareStatement(sql);
	
	//设value的值 下标为第几个~~1 2 3
	ps.setString(1, name);
	ps.setInt(2, id);
	int counter=ps.executeUpdate();
	System.out.println(counter);
	System.out.println("操作成功...");
	
	//关闭  先关语句，再关连接
	JDBCUtil.connectionCloseAndStatementColse(ps, connection);
	return counter;
}

}


