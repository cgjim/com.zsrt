package com.szrt.banji09.maven_project03.homework.day1025;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
/**
 * list
 * id  name  salary birthday
 * @author Administrator
 * 搞1个泛型的list  用 json打印
 */
public class TeacherEntity {

	@Test
	public void test() {
		StudentEntity teacher01=new StudentEntity(1,"黄老师",25000,"1991-02-10");
		StudentEntity teacher02=new StudentEntity(2,"王老师",25000,"1992-02-10");
		
		List<StudentEntity> list=new ArrayList<StudentEntity>();
		list.add(teacher01);
		list.add(teacher02);
		
		System.out.println(FanXingDemo.toJson(list));
	}

	/**
	 * map
	 * Map<Integer,TeacherEntity>
	目的：可以通过老师的id快速找到老师的所有信息  用 json打印
	 */
	@Test
	public void test2() {
		StudentEntity teacher01=new StudentEntity(1,"黄老师",25000,"1991-02-10");
		StudentEntity teacher02=new StudentEntity(2,"王老师",25000,"1992-02-10");
		
		Map<Integer,StudentEntity> map=new HashMap<Integer,StudentEntity>();
		
		map.put(01, teacher01);
		map.put(02, teacher02);
		
		//调用方法
		fingNameById(map,02);
		//StudentEntity a=map.get(01);
		//System.out.println(FanXingDemo.toJson(a));
		
	}
	
	public StudentEntity fingNameById(Map<Integer,StudentEntity> map,int id){
		//get()：获得K 返回value
		StudentEntity a=map.get(id);
		System.out.println(FanXingDemo.toJson(a));
		return null;
	}

}
