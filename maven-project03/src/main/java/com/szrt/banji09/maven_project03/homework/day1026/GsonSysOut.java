package com.szrt.banji09.maven_project03.homework.day1026;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.google.gson.Gson;
import com.szrt.banji09.maven_project03.day1025.GsonUtil;

public class GsonSysOut<T> {
/**
 * String sql2="SELECT * FROM ca_student_t ";
		ResultSet rs=statement.executeQuery(sql2);
 * @param rs
 * @return
 * @throws Exception
 */
	//传入结果
	public static<T> T sysOut(ResultSet rs) throws Exception {
		
		while(rs.next()){
			String studentName = rs.getString("STUDENT_NAME");
			int id  = rs.getInt("ID");
			int studentAge = rs.getInt("STUDENT_AGE");
			//int id, int studentAge, String studentName  TeacherEntity
			StudentEntity student = new StudentEntity(id,studentAge,studentName);
			//GsonUtil.toJson(student);
			Gson json = new Gson();
			
			System.out.println(json.toJson(student));
			
		}
		return null;
	}

}
