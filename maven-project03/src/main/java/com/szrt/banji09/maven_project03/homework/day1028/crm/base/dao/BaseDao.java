package com.szrt.banji09.maven_project03.homework.day1028.crm.base.dao;

import java.sql.SQLException;

public interface BaseDao<T> {
	/**
	 * 根据手机号码查询出员工信息
	 */
	void findEmploeeByPhone(String phone) throws Exception;
	/**
	 * 2：查询员工表中一共有多少个员工
	 * @throws SQLException 
	 */
	void selectHowMuchEmploee() throws SQLException;
	
	/**
	 * 已知页数查询
	 */
	
	void selectEmploeeLimtPage() throws Exception;
	/**
	 * 通过ID批量删除
	 * @param id1
	 * @param id2
	 * @param id3
	 * @throws SQLException
	 */
	void deleteEmploees(int id1, int id2, int id3) throws SQLException;
}
