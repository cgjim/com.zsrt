package com.szrt.banji09.maven_project03.homework.day1027.oa.emploee.entity;
/*
 * ID   NAME    PHONE  SALARY DEPARTMENT_ID
 */
public class EmploeEntity {
	private int eId;
	private String eName;
	private String phone;
	private double salary;
	private int dId;
	private String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int geteId() {
		return eId;
	}
	public void seteId(int eId) {
		this.eId = eId;
	}
	public String geteName() {
		return eName;
	}
	public void seteName(String eName) {
		this.eName = eName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public int getdId() {
		return dId;
	}
	public void setdId(int dId) {
		this.dId = dId;
	}
	public EmploeEntity(int eId,String eName, String phone, double salary, int dId, String password) {
		super();
		this.eId=eId;
		this.eName = eName;
		this.phone = phone;
		this.salary = salary;
		this.dId = dId;
		this.password = password;
	}
	
	public EmploeEntity(String phone,String password){
		super();
		this.phone = phone;
		this.password = password;
	}
	
}
