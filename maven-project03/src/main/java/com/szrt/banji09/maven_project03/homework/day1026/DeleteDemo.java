package com.szrt.banji09.maven_project03.homework.day1026;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import org.junit.Test;

public class DeleteDemo {
	/**
	 * 删除
	 * @throws Exception
	 */
	@Test
	public void delete() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		//url 连接数据库
		String url="jdbc:mysql://127.0.0.1:3306/db1610";
		String user="root";
		String password="root";
		//获取连接，登陆
		Connection connection=DriverManager.getConnection(url, user,password);
		//创建语句方法
		Statement statement=connection.createStatement();
		//插入sql语句
		int id=4;
		String sql="DELETE FROM ca_student_t WHERE id="+id;
		//执行更新方法
		statement.executeUpdate(sql);
		//System.out.println(sql);
		int counter =statement.executeUpdate(sql);
		System.out.println(counter+1);
		System.out.println("删除成功.....");
		//sql查询语句
		String sql2="SELECT * FROM ca_student_t ";
		ResultSet rs=statement.executeQuery(sql2);
		
		GsonSysOut.sysOut(rs);
	}
	/**
	 * 插入
	 * @throws Exception
	 */
	@Test
	public void insert() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		//String url="jdbc:mysql://127.0.0.1:3306/db1610";
		String url="jdbc:mysql://127.0.0.1:3306/db1610?useUnicode=true&amp;characterEncoding=UTF-8";
		String user="root";
		String password="root";
		Connection connection=DriverManager.getConnection(url, user,password);
		Statement statement=connection.createStatement();
		System.out.println("请输入ID");
		Scanner sc = new Scanner(System.in);
		int id=sc.nextInt();
		String sql="INSERT INTO ca_student_t VALUE("+id+",'陈光进',23)";
		//执行插入语句
		statement.executeUpdate(sql);
		String sql2="SELECT * FROM ca_student_t ";
		ResultSet rs=statement.executeQuery(sql2);
		GsonSysOut.sysOut(rs);
	}
}
