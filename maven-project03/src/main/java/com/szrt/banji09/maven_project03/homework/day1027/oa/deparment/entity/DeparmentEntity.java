package com.szrt.banji09.maven_project03.homework.day1027.oa.deparment.entity;

public class DeparmentEntity {
	private Integer dId;
	private String dName;
	private String dLocation;

	public Integer getdId() {
		return dId;
	}
	public void setdId(Integer dId) {
		this.dId = dId;
	}
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public String getdLocation() {
		return dLocation;
	}
	public void setdLocation(String dLocation) {
		this.dLocation = dLocation;
	}
	public DeparmentEntity(Integer dId, String dName, String dLocation) {
		super();
		this.dId = dId;
		this.dName = dName;
		this.dLocation = dLocation;
	}
	public DeparmentEntity(String dName,Integer dId) {
		super();
		this.dId = dId;
		this.dName = dName;
	
}
}
