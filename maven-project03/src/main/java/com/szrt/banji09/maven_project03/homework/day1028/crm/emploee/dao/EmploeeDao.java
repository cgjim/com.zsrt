package com.szrt.banji09.maven_project03.homework.day1028.crm.emploee.dao;

import com.szrt.banji09.maven_project03.homework.day1028.crm.base.dao.BaseDao;
import com.szrt.banji09.maven_project03.homework.day1028.crm.emploee.entity.EmploeeEntity;

public interface EmploeeDao extends BaseDao<EmploeeEntity> {

}
