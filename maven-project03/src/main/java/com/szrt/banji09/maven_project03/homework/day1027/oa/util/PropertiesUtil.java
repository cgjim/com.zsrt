package com.szrt.banji09.maven_project03.homework.day1027.oa.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
	
	public static String findValueByKey(String key) throws IOException{
		//流进入配置文件   哪里 
		InputStream input=PropertiesUtil.class.getResourceAsStream("/jdbc.properties");
		//读取JDBC配置文件  谁读
		Properties properties=new Properties();
		//进入读取
		properties.load(input);
		//接 properties 
		String value=properties.getProperty(key);
		//System.out.println(value);
		return value;
	}
}
