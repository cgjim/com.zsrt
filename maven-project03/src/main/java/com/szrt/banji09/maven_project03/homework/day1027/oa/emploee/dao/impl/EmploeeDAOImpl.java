package com.szrt.banji09.maven_project03.homework.day1027.oa.emploee.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao.impl.BaseDaoImpl;
import com.szrt.banji09.maven_project03.homework.day1027.oa.emploee.dao.EmploeeDAO;
import com.szrt.banji09.maven_project03.homework.day1027.oa.emploee.entity.EmploeEntity;
import com.szrt.banji09.maven_project03.homework.day1027.oa.util.JDBCUtil;

public   class EmploeeDAOImpl extends BaseDaoImpl<EmploeEntity> implements EmploeeDAO{

	@Override
	public int add(EmploeEntity entity) throws Exception {
		Connection connection=getConnection(); 
		String sql ="insert into oa_employee_t() values(?,?,?,?,?,?)";
		System.out.println(sql);
		PreparedStatement ps=connection.prepareStatement(sql);
		int id=entity.geteId();
		String name=entity.geteName();
		String phone=entity.getPhone();
		double salary=entity.getSalary();
		int dId=entity.getdId();
		String ePassword=entity.getPassword();
		//设value的值 下标为第几个~~1 2 3
		ps.setInt(1, id);
		ps.setString(2, name);
		ps.setString(3, phone);
		ps.setDouble(4, salary);
		ps.setInt(5, dId);
		ps.setString(6, ePassword);
		
		int counter=ps.executeUpdate();
		System.out.println(counter);
		System.out.println("操作成功...");
		
		//关闭  先关语句，再关连接
		JDBCUtil.connectionCloseAndStatementColse(ps, connection);
		return counter;
	}

	@Override
	public void select() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int alter(EmploeEntity entity) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int alter2(String name, int id) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void login(EmploeEntity entity) throws SQLException {
		Connection connection=getConnection(); 
		String sql ="SELECT * FROM oa_employee_t WHERE PHONE=? AND PASSWORD=?";
		PreparedStatement ps=connection.prepareStatement(sql);
		String phone=entity.getPhone();
		String password=entity.getPassword();
		//设value的值 下标为第几个~~1 2 3
		ps.setString(1, phone);
		ps.setString(2, password);

		ResultSet rs=ps.executeQuery();
//int eId, String eName, String phone, double salary, int dId, String password)
		boolean falg=false;
	
		while(rs.next()){
			System.out.println("登陆成功！！");
			int id=rs.getInt("ID");
			String name=rs.getString("NAME");
			String lo=rs.getString("PHONE");
			double salary=rs.getDouble("SALARY");
			int dId=rs.getInt("DEPARTMENT_ID");
			String ePassword=rs.getString("PASSWORD");
			EmploeEntity ee=new EmploeEntity(id, name, lo, salary, dId, ePassword);
			//用Gson的toJson输出 将地址改为结果
			//循环一次输出一次
			falg=true;
			Gson json=new Gson();
			System.out.println(json.toJson(ee));
		}
		
		if(!falg){
			System.out.println("登陆不成功！");
		}
		//关闭  先关语句，再关连接
		JDBCUtil.connectionCloseAndStatementColse(ps, connection);
	}



}
