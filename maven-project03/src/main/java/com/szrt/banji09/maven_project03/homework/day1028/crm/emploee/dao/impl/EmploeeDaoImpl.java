package com.szrt.banji09.maven_project03.homework.day1028.crm.emploee.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.szrt.banji09.maven_project03.homework.day1027.oa.emploee.entity.EmploeEntity;
import com.szrt.banji09.maven_project03.homework.day1027.oa.util.JDBCUtil;
import com.szrt.banji09.maven_project03.homework.day1028.crm.base.dao.impl.BaseDaoImpl;
import com.szrt.banji09.maven_project03.homework.day1028.crm.emploee.dao.EmploeeDao;
import com.szrt.banji09.maven_project03.homework.day1028.crm.emploee.entity.EmploeeEntity;

public class EmploeeDaoImpl extends BaseDaoImpl<EmploeeEntity> implements EmploeeDao{
/**
 * 通过手机号码找员工信息
 */
	@Override
	public void findEmploeeByPhone(String phone) throws Exception {
		Connection connection=getConnection(); 
		String sql ="SELECT * FROM cgj_employee_t WHERE PHONE=?";
		PreparedStatement ps=connection.prepareStatement(sql);
		//设value的值 下标为第几个~~1 2 3
		ps.setString(1, phone);
		ResultSet rs=ps.executeQuery();
		while(rs.next()){
			int id=rs.getInt("ID");
			String name=rs.getString("NAME");
			String ePhone=rs.getString("PHONE");
			double salary=rs.getDouble("SALARY");
			int dId=rs.getInt("DEPARTMENT_ID");
			String ePassword=rs.getString("PASSWORD");
			EmploeEntity ee=new EmploeEntity(id, name, ePhone, salary, dId, ePassword);
			//用Gson的toJson输出 将地址改为结果
			//循环一次输出一次
			Gson json=new Gson();
			System.out.println(json.toJson(ee));
		}
		
		//关闭  先关语句，再关连接
		JDBCUtil.connectionCloseAndStatementColse(ps, connection);
	}

	@Override
	public void selectHowMuchEmploee() throws SQLException {
		Connection connection=getConnection(); 
		String sql ="SELECT COUNT(*) FROM cgj_employee_t";
		PreparedStatement ps=connection.prepareStatement(sql);
		ResultSet rs=ps.executeQuery(sql);
		System.out.println(rs);
		System.out.println("操作成功...");
	}
/**
 * 批量删除
 */
	@Override
	public void deleteEmploees(int id1,int id2,int id3) throws SQLException {
		Connection connection=getConnection(); 
		String sql ="DELETE FROM cgj_employee_t WHERE ID IN(?,?,?)";
		PreparedStatement ps=connection.prepareStatement(sql);
		ps.setInt(1, id1);
		ps.setInt(2, id2);
		ps.setInt(3, id3);
		int counter=ps.executeUpdate();
		System.out.println(counter);
		System.out.println("操作成功...");
		
	}
/**
 * 分页查询
 */
	@Override
	public void selectEmploeeLimtPage() throws Exception {
		Connection connection=getConnection(); 
		String sql ="SELECT * FROM cgj_employee_t  LIMIT 0,4  ";
	}
/**
 * 登陆
 */
	
}
