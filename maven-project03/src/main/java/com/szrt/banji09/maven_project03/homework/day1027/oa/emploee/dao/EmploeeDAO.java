package com.szrt.banji09.maven_project03.homework.day1027.oa.emploee.dao;

import java.sql.SQLException;

import com.szrt.banji09.maven_project03.homework.day1027.oa.base.dao.BaseDao;
import com.szrt.banji09.maven_project03.homework.day1027.oa.emploee.entity.EmploeEntity;

public interface EmploeeDAO extends BaseDao<EmploeEntity>{
	/**
	 * 登陆 ，这里通过手机和密码登陆
	 * @throws SQLException 
	 */
	void login(EmploeEntity entity) throws SQLException;
}
