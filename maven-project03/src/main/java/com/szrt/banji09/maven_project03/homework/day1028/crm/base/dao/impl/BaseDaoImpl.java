package com.szrt.banji09.maven_project03.homework.day1028.crm.base.dao.impl;

import java.sql.Connection;

import com.szrt.banji09.maven_project03.homework.day1027.oa.util.JDBCUtil;
import com.szrt.banji09.maven_project03.homework.day1028.crm.base.dao.BaseDao;

public abstract class BaseDaoImpl<T> implements BaseDao<T>{

	private static Connection connection=null;
	static{
		try {
			connection=JDBCUtil.ConnectionDB();
			System.out.println(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection() {
		return connection;
	}
	

}
