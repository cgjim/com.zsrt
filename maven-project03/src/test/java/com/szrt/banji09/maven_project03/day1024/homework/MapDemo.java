package com.szrt.banji09.maven_project03.day1024.homework;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class MapDemo {
/**
 * map   key value  用泛型  [7:12]

	  10   陈叶 
	  1: 把这5个人放在map
	  1： 拿出 周芷若 

 */
	@Test
	public void test() {
		Map<String, String> childMap=new HashMap<String,String>();
		childMap.put("10", "陈叶");
		childMap.put("11", "杨越");
		childMap.put("12", "小昭");
		childMap.put("13", "周芷若");
		childMap.put("14", "张无忌");
		
		String name=childMap.get("13");
		System.out.println(name);
	}

}
