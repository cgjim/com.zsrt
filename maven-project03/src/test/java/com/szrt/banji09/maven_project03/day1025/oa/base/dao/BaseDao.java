package com.szrt.banji09.maven_project03.day1025.oa.base.dao;

public interface BaseDao<T> {
	int add(T t);
	int delete(int id);
	int update(T t);
	T findById(int id);
}
