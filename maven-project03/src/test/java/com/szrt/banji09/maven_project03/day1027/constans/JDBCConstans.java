package com.szrt.banji09.maven_project03.day1027.constans;

public interface JDBCConstans {
	/**
	 * 1 接口默认 前缀
	 * public static final 
	 * 
	 * 2 连接数据库的用户名
	 * 常量一般来说 ：大写
	 * 
	 */
	
	String USER="user";
	String PASSWORD="password";
	String URL="url";
	String DRIVER="driver";
	
}
