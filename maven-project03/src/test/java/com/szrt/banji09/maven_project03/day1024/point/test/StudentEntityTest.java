package com.szrt.banji09.maven_project03.day1024.point.test;

import org.junit.Test;

import com.google.gson.Gson;
import com.szrt.banji09.maven_project03.day1024.point.entity.StudentEntity;

public class StudentEntityTest {

	@Test
	public void test() {
		StudentEntity cy=new StudentEntity();
		cy.setId(10);
		cy.setStudentName("陈叶");
		cy.setSex("男");
		cy.setAge(23);
		
		System.out.println(cy);
		Gson json=new Gson();
		String entity=json.toJson(cy);
		System.out.println(entity);
	}

}
