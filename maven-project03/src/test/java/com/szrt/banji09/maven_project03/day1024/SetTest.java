package com.szrt.banji09.maven_project03.day1024;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

public class SetTest {

	/**
	 * set 的东西不重复 ,重复会覆盖
	 */
	@Test
	public void test() {
		Set<String> set=new HashSet<String>();
		
		set.add("10");
		set.add("20");
		set.add("10");
		System.out.println(set.size());
	
		//遍历 1 从头到尾元素都 可以访问到 next
		//   2 可以知道什么结束 起止范围  hasNext
		//   3 步长+1
		//Iterator 迭代器=遍历器
		Iterator<String> it=set.iterator();
		
		while(it.hasNext()){
			
			String value= it.next();
			System.out.println(value);
		}
	
	}

}
