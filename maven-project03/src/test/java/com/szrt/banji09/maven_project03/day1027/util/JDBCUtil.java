package com.szrt.banji09.maven_project03.day1027.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.szrt.banji09.maven_project03.day1027.constans.JDBCConstans;

public class JDBCUtil {
	
	public static Connection ConnectionDB() throws Exception{
		Class.forName(PropertiesUtil.findValueByKey(JDBCConstans.DRIVER));
		//String url="jdbc:mysql://127.0.0.1:3306/db1610";
		String url=PropertiesUtil.findValueByKey(JDBCConstans.URL);
		String user=PropertiesUtil.findValueByKey(JDBCConstans.USER);
		String password=PropertiesUtil.findValueByKey(JDBCConstans.PASSWORD);
		Connection connection=DriverManager.getConnection(url, user,password);
		return connection;
	}
	
	
}
