package com.szrt.banji09.maven_project03.day1025.crm.student.service;

import com.szrt.banji09.maven_project03.day1025.crm.base.service.BaseService;
import com.szrt.banji09.maven_project03.day1025.student.entity.StudentEntity;

public interface StudentService extends BaseService<StudentEntity>{

}
