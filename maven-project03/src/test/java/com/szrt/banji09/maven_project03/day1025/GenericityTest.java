package com.szrt.banji09.maven_project03.day1025;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.szrt.banji09.maven_project03.day1025.entity.StudentEntity;

public class GenericityTest {
//  篮子放组长的名字  String
	@Test
	public void test() {
		List<String> projectLeaders=new ArrayList<String>();
		projectLeaders.add("陈杨森");
		projectLeaders.add("曹耿晖");
		projectLeaders.add("所志松");
		projectLeaders.add("郑智强");
		projectLeaders.add("曾瑶");
		System.out.println(projectLeaders);
		System.out.println(GsonUtil.toJson(projectLeaders));
	}
	/**
	 * Integer id, String studentName, Integer age, String sex
	 */
	@Test
	public void test01() {
		StudentEntity zzq=new StudentEntity(10,"郑智强",23,"男");
		StudentEntity cys=new StudentEntity(11,"陈杨森",23,"男");
		StudentEntity cgh=new StudentEntity(12,"曹耿晖",23,"男");
		StudentEntity rzs=new StudentEntity(13,"所志松",23,"男");
		StudentEntity zy=new StudentEntity(14,"曾瑶",23,"男");
		
		List<StudentEntity> list=new ArrayList<StudentEntity>();
		list.add(zzq);
		list.add(cys);
		list.add(cgh);
		list.add(rzs);
		list.add(zy);
		
		System.out.println(GsonUtil.toJson(list));
		System.out.println(list.toString());
	}
}
