package com.szrt.banji09.maven_project03.day1025;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.google.gson.Gson;

public class DateTest {

	@Test
	public void test() {
		//这个构造方法过时
		Date date = new Date(2016,9,25);
		Date date1 = new Date(2016,9,25);
		Gson json = new Gson();
		//得到当前系统时间
		System.out.println(json.toJson(date1));
	}
	/**
	 * 自定义时间格式   SimpleDateFormat
	 */
	@Test
	public void test01() {
		Date date = new Date();
		//可以自定义格式
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd hh:mm:s:S a");
		SimpleDateFormat sdf02 = new SimpleDateFormat("yy年MM月dd日 hh时mm分s秒  a");
		String d=sdf.format(date);
		String h=sdf02.format(date);
		System.out.println(d);
		System.out.println(h);
		
	}
	/**
	 * 把String转化在date
	 * @throws ParseException 
	 * CST :center Starder Time
	 */
	@Test
	public void test02() throws ParseException {
		String a="16-10-25 10:28:15";
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd hh:mm:s");
		Date date=sdf.parse(a);
		System.out.println(date);
	}
}
