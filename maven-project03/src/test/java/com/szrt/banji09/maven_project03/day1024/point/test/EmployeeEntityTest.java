package com.szrt.banji09.maven_project03.day1024.point.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;
import com.szrt.banji09.maven_project03.day1024.point.entity.EmployeeEntity;

public class EmployeeEntityTest {
/**
 * Integer id, String name, double salary, String birthday, String dateTime, String phone
 * gson 
 */
	@Test
	public void test() {
		//弄构造函数  代替set
		EmployeeEntity zzq=new EmployeeEntity(1,"郑智强",9000,"1993-03-22","2016-10-19","12561388559");
		EmployeeEntity cys=new EmployeeEntity(2,"陈杨森",8000,"1993-08-22","2016-10-19","13461388559");
		
		Gson json=new Gson();
		String entity=json.toJson(zzq);
		System.out.println(entity);
		
		System.out.println("========");
		//用list
		List<EmployeeEntity> list=new ArrayList<EmployeeEntity>();
		//增加对象
		list.add(zzq);
		list.add(cys);
		String entityList =json.toJson(list);
		System.out.println(entityList);
		
	}

}
