package com.szrt.banji09.maven_project03.day1025.oa.emploe.dao;

import com.szrt.banji09.maven_project03.day1025.oa.base.dao.BaseDao;
import com.szrt.banji09.maven_project03.day1025.oa.emploe.entity.EmploeeEntity;

public interface EmploeDao extends BaseDao<EmploeeEntity>{
	
	String findIdByPhone();
	
}
