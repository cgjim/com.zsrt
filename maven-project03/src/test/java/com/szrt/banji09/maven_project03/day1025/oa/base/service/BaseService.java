package com.szrt.banji09.maven_project03.day1025.oa.base.service;

public interface BaseService<T> {
	int add(T t);
	int delete(int id);
	int update(T t);
	T findById(int id);
}
