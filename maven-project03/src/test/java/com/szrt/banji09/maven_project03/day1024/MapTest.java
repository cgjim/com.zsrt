package com.szrt.banji09.maven_project03.day1024;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class MapTest {
	/**
	 * map 增加
	 *
	 */
	@Test
	public void test() {
		List<String> list=new ArrayList<String>();
		Map<String, String> childMap=new HashMap<String,String>();
		childMap.put("xzr", "黄梓睿");
		childMap.put("cgj", "陈光进");
		
		System.out.println(childMap.size());
		//get 通过KEY 取得value
		String name=childMap.get("cgj");
		System.out.println(name);
	}
	
	/**
	 * 映射
	 */
	@Test
	public void test01() {
		Map<String, String> childMap=new HashMap<String,String>();
		childMap.put("xzr", "黄梓睿");
		childMap.put("cgj", "陈光进");
		
		Map<String, String> Map=new HashMap<String,String>();
		
	}
	
	@Test
	public void test02() {
		Map<String, String> childMap=new HashMap<String,String>();
		childMap.put("10", "陈叶");
		childMap.put("11", "杨越");
		childMap.put("12", "小昭");
		childMap.put("13", "周芷若");
		childMap.put("14", "张无忌");
		String name=childMap.get("13");
		System.out.println(name);
		
		//.value() 返回map的value
		System.out.println(childMap.values());
		//.ketSet() 返回map的key
		System.out.println("==key的值==");
		System.out.println(childMap.keySet());
	}
	/**
	 * map遍历
	 */
	@Test
	public void test03() {
		Map<String, String> childMap=new HashMap<String,String>();
		childMap.put("10", "陈叶");
		childMap.put("11", "杨越");
		childMap.put("12", "小昭");
		childMap.put("13", "周芷若");
		childMap.put("14", "张无忌");
		
		for(String a:childMap.keySet()){
			System.out.println(a+"\t 值 "+childMap.get(a));
		}
		
	}
	/**
	 * 删除
	 */
	@Test
	public void test04() {
		Map<String, String> childMap=new HashMap<String,String>();
		childMap.put("10", "陈叶");
		childMap.put("11", "杨越");
		childMap.put("12", "小昭");
		childMap.put("13", "周芷若");
		childMap.put("14", "张无忌");
		//删除
		childMap.remove(13);
		
		for(String a:childMap.keySet()){
			System.out.println(a+"\t 值 "+childMap.get(a));
		}
		
	}
	
	@Test
	public void test05() {
		Map<String, String> childMap=new HashMap<String,String>();
		childMap.put("10", "陈叶");
		childMap.put("11", "杨越");
		childMap.put("12", "小昭");
		childMap.put("13", "周芷若");
		childMap.put("14", "张无忌");
		boolean z=false;
		for(String a:childMap.keySet()){
			if(childMap.get(a).equals("周芷若")){
				z=true;
				System.out.println(z);
			}
			//System.out.println(a+"\t 值 "+childMap.get(a));
		}
		if(z==false){
			System.out.println(z);
		}
		
	}
}
