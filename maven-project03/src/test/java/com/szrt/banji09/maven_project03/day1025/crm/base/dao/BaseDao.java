package com.szrt.banji09.maven_project03.day1025.crm.base.dao;

public interface BaseDao<T> {
	
	int insert(T entity);
}
