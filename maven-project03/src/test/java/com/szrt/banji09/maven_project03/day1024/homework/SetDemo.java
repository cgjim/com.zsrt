package com.szrt.banji09.maven_project03.day1024.homework;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

public class SetDemo {
	/**
	 *  请举例说明 Set<String> 加入3个元素
	 *  removeAll 的用法
	 */
	@Test
	public void test() {
		Set<String> set=new HashSet<String>();
		set.add("11");
		set.add("22");
		set.add("33");
		Iterator<String> a=set.iterator();
		//删除所有
		//set.removeAll(set);
	/*	while(a.hasNext()){
			String value= a.next();
			System.out.println(value);
		}*/
		//remove set中的指定元素
		set.remove("11");
		System.out.println("第二种循环");
		for(String b:set){
			System.out.println(b);
		}
		
	}

}
