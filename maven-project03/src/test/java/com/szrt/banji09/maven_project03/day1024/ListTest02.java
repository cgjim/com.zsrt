package com.szrt.banji09.maven_project03.day1024;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListTest02 {

	/**
	 * list 有序的
	 */
	@Test
	public void test() {
		List list=new ArrayList<>();
		//集合的长度
		int length=list.size();
		System.out.println(length);
		list.add("10");
		list.add("20");
		list.add(1);
		//获取list位置
		System.out.println(list.get(0));
		System.out.println(list.get(2));
		System.out.println(length);
	}
	
	@Test
	public void test02(){
		//泛形
		List<String> list=new ArrayList<String>();
		list.add("11");
		list.add("22");
		list.add("33");
		//list的循环（普通）
		for(int index=0; index<list.size();index++){
			System.out.println(list.get(index));
		}
		System.out.println("====");
		//高级循环  新定义的对象：遍历的数组 
		for(String index : list){
			System.out.println(index);
		}
		//查找list 内容的下标（索引）
		System.out.println(list.indexOf("22"));
		//删除下标
		System.out.println("删除");
		list.remove(1);
		for(String index : list){
			System.out.println(index);
		}
	}

}
