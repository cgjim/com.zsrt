package com.szrt.banji09.maven_project03.day1024.point02;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringTest {
/**
 * String 
 */
	@Test
	public void test() {
		String name="张无忌";
		//多少个字符？ 一个字一个字符！
		int length=name.length();
		System.out.println(length);
		
		//遍历
		for(int i=0;i<name.length();i++){
			//.charAt 字符在1 则返回char 
			char a=name.charAt(i);
			System.out.println(a);
		}
		//遍历2
		for(char a:name.toCharArray()){
			System.out.println(a);
		}
	}
/**
 * 字符串截取
 */
	@Test
	public void test02() {
		String name="张无忌爱大大";
		//.charAt 字符在0 则返回char  name[0]=张 
		System.out.println(name.charAt(0));
		//字符串截取
		System.out.println(name.substring(1));
		//从1到3  右边不包括本身
		System.out.println(name.subSequence(1, 4));
	}
	/**
	 * spilt
	 */
	@Test
	public void test03() {
		String name="陈光进#瓶子#呵呵#嘻嘻";
		//以#分隔开，并变成数组
		String[] name01=name.split("#");
		
		for(String a:name01){
			System.out.print(a +"\t");
		}
		
	}
	@Test
	public void test04() {
		
	}
}
