package com.szrt.banji09.maven_project03.day1024.homework;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class ListDemo {
/**
 * list 有序的可重复的数组
 * .size() 取大小长度
 * .
 */
	@Test
	public void test() {
		List<String> list=new LinkedList<String>();
		//长度 可重复
		list.add("10");
		list.add("10");
		list.add("10");
		int len=list.size();
		System.out.println(len);
		
	}
	/**
	 * list一般将泛型<>写上，定义类型
	 */
	@Test
	public void test02() {
		List list=new LinkedList();
		//长度 可重复
		list.add("10");
		list.add("10");
		list.add("10");
		int len=list.size();
		System.out.println(len);
		String ss=(String) list.get(1);
		System.out.println(ss);
	}
	/**
	 * 遍历
	 */
	@Test
	public void test03() {
		List<String> list=new LinkedList<String>();
		//长度 可重复
		list.add("10");
		list.add("10");
		list.add("15");
		list.add("20");
		for(int i=0;i<list.size();i++){
			System.out.println(list.get(i));
		}
		list.remove(list.get(2));
		System.out.println("高级for");
		for(String a:list){
			System.out.println(a);
		}
	}
}
