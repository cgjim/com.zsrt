package com.szrt.banji09.maven_project03.day1025.crm.base.service;

public interface BaseService<T> {
	int add(T entity);
	/**

	 * 根据id删除1条记录

	 * @param id

	 * @return

	 */
	int delete(int id);
	
	/**
	 * 在entity 肯定会指定id 千万不要做批量修改！
	 * @param entity
	 * @return
	 */
	int update(T entity);
	
	/**

	 * 根据id查找到1条记录   1个对象

	 * @param id

	 * @return

	 */
	T findById(int id);
}
