package com.szrt.banji09.maven_project03.day1025;

import com.google.gson.Gson;
/**
 * 泛型
 * <抽象的类型>  K T V 
 * K：key  T:type  V:value 
 * 
 * @author Administrator
 *
 * @param <T>
 */
public class GsonUtil<T> {
	//泛型静态
	public static<T> String toJson(T xx){
		Gson json = new Gson();
		
		return json.toJson(xx);
	}
}
