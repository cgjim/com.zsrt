package com.szrt.banji09.maven_project03.day1024.point.entity;
/**
 * ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `NAME` varchar(128) DEFAULT NULL COMMENT '姓名',
  `EMPLOY_WAGE` decimal(10,2) DEFAULT NULL COMMENT '员工工资',
  `BIRTHDAY` date DEFAULT NULL COMMENT '生日',
  `LOGIN_TIME` datetime DEFAULT NULL COMMENT '登录时间',
  `CELL_PHONE` varchar(11) DEFAULT NULL COMMENT '手机号码',
  PRIMARY KEY (`ID`)
 * @author Administrator
 *
 */
public class EmployeeEntity {
	private Integer id;
	private String name;
	private double salary;
	private String birthday;
	private String dateTime;
	private String phone ;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public EmployeeEntity(Integer id, String name, double salary, String birthday, String dateTime, String phone) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.birthday = birthday;
		this.dateTime = dateTime;
		this.phone = phone;
	}
	
}
