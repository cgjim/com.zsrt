package com.szrt.banji09.maven_project03.day1027;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import org.junit.Test;

import com.szrt.banji09.maven_project03.day1027.constans.JDBCConstans;
import com.szrt.banji09.maven_project03.day1027.util.JDBCUtil;
import com.szrt.banji09.maven_project03.day1027.util.PropertiesUtil;
import com.szrt.banji09.maven_project03.homework.day1026.GsonSysOut;

public class MySQLDemo {

	/***
	 * 读取配置文件   
	 * @throws Exception
	 */
	@Test
	public void insert() throws Exception {
		/*Class.forName(PropertiesUtil.findValueByKey(JDBCConstans.DRIVER));
		//String url="jdbc:mysql://127.0.0.1:3306/db1610";
		String url=PropertiesUtil.findValueByKey(JDBCConstans.URL);
		String user=PropertiesUtil.findValueByKey(JDBCConstans.USER);
		String password=PropertiesUtil.findValueByKey(JDBCConstans.PASSWORD);
		Connection connection=DriverManager.getConnection(url, user,password);*/
		
		//调用连连接数据库的方法
		Connection connection=JDBCUtil.ConnectionDB();
		Statement statement=connection.createStatement();
		System.out.println("请输入ID");
		Scanner sc = new Scanner(System.in);
		int id=sc.nextInt();
		String sql="INSERT INTO ca_student_t VALUE("+id+",'陈光进',23)";
		//执行插入语句
		statement.executeUpdate(sql);
		String sql2="SELECT * FROM ca_student_t ";
		ResultSet rs=statement.executeQuery(sql2);
		GsonSysOut.sysOut(rs);
	}
	
	@Test
	public void delete() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		//url 连接数据库
		String url="jdbc:mysql://127.0.0.1:3306/db1610";
		String user="root";
		String password="root";
		//获取连接，登陆
		Connection connection=DriverManager.getConnection(url, user,password);
		//创建语句方法
		Statement statement=connection.createStatement();
		//插入sql语句
		int id=4;
		String sql="DELETE FROM ca_student_t WHERE id="+id;
		//执行更新方法
		statement.executeUpdate(sql);
		//System.out.println(sql);
		int counter =statement.executeUpdate(sql);
		System.out.println(counter+1);
		System.out.println("删除成功.....");
		//sql查询语句
		String sql2="SELECT * FROM ca_student_t ";
		ResultSet rs=statement.executeQuery(sql2);
		
		//
		GsonSysOut.sysOut(rs);
	}
}
