package com.szrt.banji09.maven_project03.day1025.student.entity;

public class StudentEntity {
	private Integer id;
	private String StudentName;
	private Integer age;
	private String sex;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStudentName() {
		return StudentName;
	}
	public void setStudentName(String studentName) {
		StudentName = studentName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public StudentEntity(Integer id, String studentName, Integer age, String sex) {
		super();
		this.id = id;
		StudentName = studentName;
		this.age = age;
		this.sex = sex;
	}
	public StudentEntity(){
		
	}
}
