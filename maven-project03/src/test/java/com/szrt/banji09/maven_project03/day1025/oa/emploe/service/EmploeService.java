package com.szrt.banji09.maven_project03.day1025.oa.emploe.service;

import com.szrt.banji09.maven_project03.day1025.crm.base.service.BaseService;
import com.szrt.banji09.maven_project03.day1025.oa.emploe.entity.EmploeeEntity;

public interface EmploeService extends BaseService<EmploeeEntity>{
	String findNameByPhone(String phone);
}
