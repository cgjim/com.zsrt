package com.szrt.banji09.maven_project03.day1024.homework;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class RemoveallTest {
/**
 *  removeAll 的用法 
 */
	@Test
	public void test() {
		List<String> list=new ArrayList();
		list.add("10");
		list.add("20");
		list.add("30");
		//是否可以一次性删除所有?  可以
		list.removeAll(list);
		System.out.println(list);
		//不可以只删除2
		//list.removeAll(list.get(2));
		
	}

}
