package com.szrt.javaweb.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class HelloServlet implements Servlet {

	@Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("hello world!  ...inint()");
		//通过KEY得到value
		String uu=config.getInitParameter("user");
		//System.out.println(uu);
		String pp=config.getInitParameter("password");
		
		Enumeration<String> keys = config.getInitParameterNames();
		while(keys.hasMoreElements()){
			//获得KEY
			String key=keys.nextElement();
			//通过KEY获得value
			String aa=config.getInitParameter(key);
			System.out.println(key+"=="+aa);
		}
		
	}

	@Override
	public ServletConfig getServletConfig() {
		System.out.println("hello world!  getServletConfig");
		return null;
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		System.out.println("hello world!  service");
	}

	@Override
	public String getServletInfo() {
		System.out.println("hello world!  getServletInfo");
		return null;
	}

	@Override
	public void destroy() {
		System.out.println("hello world!  destroy");
	}

}
