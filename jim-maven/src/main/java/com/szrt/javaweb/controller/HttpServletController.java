package com.szrt.javaweb.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpServletController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
	}
	@Override
	public void init(ServletConfig config) {
		ServletContext servletContext = config.getServletContext();
		InputStream is=null;
		InputStreamReader isr=null;
		BufferedReader br=null;
		
		is=servletContext.getResourceAsStream("/WEB-INF/classes/jdbc.properties");
		isr=new InputStreamReader(is);
		br= new BufferedReader(isr);
		try {
			String str=null;
			while((str=br.readLine())!=null){
				System.out.println(str);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		try {
			Properties properties = new Properties(); 
			properties.load(is);
			Enumeration<?> keys = properties.keys();
			while(keys.hasMoreElements()){
				String key = (String)keys.nextElement();
				String value = properties.getProperty(key);
				System.err.println(key+"==="+value);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(br!=null){
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			if(br!=null){
				try {
					isr.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(br!=null){	
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		//String driver=properties.getProperty("driver");
	}
	
	public void init02(ServletConfig config) throws IOException {
		ServletContext servletContext = config.getServletContext();
		InputStream in01=servletContext.getResourceAsStream("/WEB-INF/classes/jdbc.properties");
		Properties properties = new Properties(); 
		
		properties.load(in01);
		//String driver=properties.getProperty("driver");
		Enumeration<?> keys = properties.keys();
		while(keys.hasMoreElements()){
			String key = (String)keys.nextElement();
			String value = properties.getProperty(key);
			System.err.println(key+"==="+value);
		}
	}
		
}
