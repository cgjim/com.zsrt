package com.szrt.javaweb.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class GenericServletController extends GenericServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//重写alt +T
	@Override
	public void init(ServletConfig config) throws ServletException {
		ServletContext servletContext = config.getServletContext();
		System.out.println(servletContext);
		Enumeration<String> keys = servletContext.getInitParameterNames();
		while(keys.hasMoreElements()){
			String key=keys.nextElement();
			String value=servletContext.getInitParameter(key);
			System.out.println(key+"========="+value);
		}
	}
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
