package com.szrt.banji09.mavenHelloWorld01;

import java.security.Security;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmail {
	private String from1;
	private String pw1;
	public void sendemail(String host,String to,String from,String pw,String subject,String content){
		from1 = from;
		pw1 = pw;
		//收件人电子邮箱
		 //to = "593455003@qq.com";
		to=to;
		
		//发件人电子邮箱
		 //from = "15818245636@163.com";
		from=from;
		//指定发送的主机smtp.qq.com
		 //host = "smtp.163.com";
		host = host;
		
	    final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		
		//获取系统属性
		Properties properties = System.getProperties();
		
		//设置邮件服务器
		properties.setProperty("mail.smtp.host", host);
		
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.smtp.auth", "true");
		//properties.setProperty("mail.user", "myuser");
		//properties.setProperty("mail.password", "mypwd");
		
		//获取默认session对象
		Session session = Session.getDefaultInstance(properties,new Authenticator(){
			public PasswordAuthentication getPasswordAuthentication(){
				//发件人邮箱和密码
				return new PasswordAuthentication(from1,pw1);
			}
		});
		
		try{
			//创建默认的MimeMessage 对象
			MimeMessage message = new MimeMessage(session);
			
			//Set From:头部头字段
			message.setFrom(new InternetAddress(from));
			
			//Set To:头部头字段
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			//Set Subject:头部头字段
			message.setSubject(subject);
			
			//设置消息体
			message.setText(content);
			
			
			//发送消息
			Transport.send(message);
			System.out.println("Send message successfully....");
		}catch(MessagingException mex){
			mex.printStackTrace();
		}
	}

}
