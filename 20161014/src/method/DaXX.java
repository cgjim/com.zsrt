package method;

import static org.junit.Assert.*;

import org.junit.Test;

public class DaXX {
/**
 * *
 * **
 * ***
 * ****  倒过来打印星
 */
	@Test
	public void test() {
		for(int i=0;i<=6;i++){
			for(int j=6;j>=i;j--){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
/**
 * 打星星1
 */
	@Test
	public void test2() {
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				if(i==0&&j==4){
					System.out.print("* ");
				}else if(i==1&&(j==2||j==3||j==4)){
					System.out.print("* ");
				}else if(i==2&&(j==1||j==2||j==3||j==4||j==5)){
					System.out.print("* ");
				}else if(i==3){
					System.out.print("* ");
				}else{
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
	/***
	 * 打星星2 菱形 
	 */
	@Test
	public void test3() {
		for(int i=0;i<7;i++){
			for(int j=0;j<6;j++){
				if(i==0&&j==5){
					System.out.print("* ");
				}else if(i==1&&(j==2||j==3||j==4)){
					System.out.print("* ");
				}else if(i==2&&(j==1||j==2||j==3||j==4||j==5)){
					System.out.print("* ");
				}else if(i==3){
					System.out.print("* ");
				}else if(i==4&&(j==1||j==2||j==3||j==4||j==5)){
					System.out.print("* ");
				}else if(i==5&&(j==2||j==3||j==4)){
					System.out.print("* ");
				}else if(i==6&&j==4){
					System.out.print("* ");
				}else{
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
	/**
	 * 打星星  长方形
	 */
	@Test
	public void test4() {
		for(int i=0;i<5;i++){
			for(int j=0;j<8;j++){
				if(i==0){
					System.out.print("* ");
				}else if(j==0){
					System.out.print("* ");
				}else if(j==7){
					System.out.print("* ");
				}else if(i==4){
					System.out.print("* ");
				}else{
					System.out.print("  ");
				}
				
			}
			System.out.println();
		}
	}
	@Test
	public void test5() {
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				if(i==0&&j==2){
					System.out.print("*");
				}else if(i==1&&(j==1||j==3)){
					System.out.print("*");
				}else if(i==2&&(j==0||j==4)){
					System.out.print("*");
				}
				else if(i==3&&(j==1||j==3)){
					System.out.print("*");
				}else if(i==4&&j==2){
					System.out.print("*");
				}
				else{
					System.out.print(" ");
				}
			}
			System.out.println();
		}
}
}
