package com.szrt.shiro.user.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
	
	@RequestMapping("/user")
	@RequiresPermissions("user:init")
	public String init(){
		return "user";
	}
	
	@RequiresPermissions("user:add")
	@RequestMapping("/adduser")
	public String add(){
		return "addUser";
	}
}
