package com.szrt.shiro.user.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;




@Controller
public class CGJUserController {
	/**
	 * 登陆
	 * @return 
	 */
	@RequestMapping("/login")
	public ModelAndView login(String uesername,String password){
		Subject currentUser = SecurityUtils.getSubject();
		ModelAndView mav = new ModelAndView("index");
        //登录过系统在指定时间内不用再次登录，具体配置参见rememberMeManager
		//其他资源的访问需要扩展其过滤器才能达到目的
		if(currentUser.isRemembered()){
			mav.setViewName("user");
			return mav;
		    }
		if( !currentUser.isAuthenticated() ) {
		//我们将使用用户名/密码的例子因为它是最常见的。.
		UsernamePasswordToken token = new UsernamePasswordToken(uesername, password);

		//支持'remember me' (无需配置，内建的!):
		token.setRememberMe(true);
		try{
			currentUser.login(token);
			mav.setViewName("user");
		}catch(AuthenticationException e){
			mav.addObject("checkerror","用户名或密码错误");
		        }
		    }
		return mav;
		}
}
