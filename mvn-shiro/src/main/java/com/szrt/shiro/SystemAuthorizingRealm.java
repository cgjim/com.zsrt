package com.szrt.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class SystemAuthorizingRealm extends AuthorizingRealm {
	
	//授权 
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		System.out.println((String)principals.getPrimaryPrincipal());
		//TODO可以访问DAO读取权限
		//权限对象
		SimpleAuthorizationInfo sai = new SimpleAuthorizationInfo();
		sai.addStringPermission("user:init");//添加权限
		sai.addStringPermission("role:add");
		sai.addStringPermission("user:add");
		return sai;

	}

	// 认证	
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		System.out.println(token.getPrincipal());
		//TODO可以写访问数据库的代码，读取密码
		String pwd = new Md5Hash("11").toHex();
		String username = (String) token.getPrincipal();		
		SimpleAuthenticationInfo sai = new SimpleAuthenticationInfo(username,pwd,this.getName());
		return sai;
	}

}
