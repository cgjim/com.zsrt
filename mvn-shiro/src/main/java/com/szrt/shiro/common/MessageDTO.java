package com.szrt.shiro.common;

import java.util.Map;

public class MessageDTO {
	
	
	/**
	 * 规定1代表操作成功   0代表操作失败
	 */
	private int flag;
	
	
	/**
	 * 具体的消息：  "部门新增成功"
	 */
	private String message;
	
	public static MessageDTO getSuccessMessage(){
		MessageDTO dto = new MessageDTO();
		dto.setFlag(1);
		dto.setMessage("操作成功...");
		return dto;
	}
	
	public static void putSuccessMessage(Map<String,Object> map){
		map.put("data", getSuccessMessage());
	}
	public static void putFailureMessage(Map<String,Object> map){
		map.put("data", getFailureMessage());
	}
	public static void putErrorMessage(Map<String,Object> map){
		map.put("data", getErrorMessage());
	}
	
	public static MessageDTO getErrorMessage(){
		MessageDTO dto = new MessageDTO();
		dto.setFlag(0);
		dto.setMessage("500...");
		return dto;
	}
	
	public static MessageDTO getFailureMessage(){
		MessageDTO dto = new MessageDTO();
		dto.setFlag(0);
		dto.setMessage("操作失败");
		return dto;
	}


	public int getFlag() {
		return flag;
	}


	public void setFlag(int flag) {
		this.flag = flag;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MessageDTO [flag=" + flag + ", message=" + message + "]";
	}
	
	
	

}
