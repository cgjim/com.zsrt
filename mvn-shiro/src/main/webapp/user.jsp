<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
 <%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
</head>
<body>
用户列表<br>
<shiro:hasPermission name="user:add">添加用户<br></shiro:hasPermission>
代码演示账号：lyc    密码：123

测试方法：
登录后进入http://localhost:8080/shiro_springmvc/user
在地址栏中录入http://localhost:8080/shiro_springmvc/adduser会没有权限访问。
</body>
</html>