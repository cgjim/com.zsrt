<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!-- 导入c 标签 -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>欢迎使用中深软通办公管理系统</title>

<%
	String context = request.getContextPath();
%>

</head>

<body>
	
	<div align="center" id="d1">
		<div id="d2" style="width: 540px; height: 350px; padding: 20px;"
			align="center">
			<form id="fm" action="login" method="post">

				<table align="center">
					<tr>
						<td><span>用户名：</span><input name="uesername" value="" /></td>
						<td><span id="message">${requestScope.message}</span></td>
					</tr>
					<tr>
						<td><span>密&nbsp;&nbsp;&nbsp;码：</span><input name="password"
							value="" type="password" /></td>
						<td><span id="userError">${requestScope.userError}</span></td>
					</tr>
					<tr id="btr">
						<td colspan="2" align="center">
						<input id="button" type="submit" value=" 登 录  " /> &nbsp;&nbsp; 
						<input id="button1" type="reset" value=" 重 置  " /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">${requestScope.checkerror}</td>
						<td></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>

</html>