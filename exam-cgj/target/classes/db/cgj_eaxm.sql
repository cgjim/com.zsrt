/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.6.0-m4 : Database - exam-ssm-1609
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `cgj_account_t` */

DROP TABLE IF EXISTS `cgj_account_t`;

CREATE TABLE `cgj_account_t` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '储户ID',
  `ACCOUNT_NAME` varchar(50) DEFAULT NULL COMMENT '储户的名称',
  `ACCOUNT_NUMBER` varchar(18) DEFAULT NULL COMMENT '储户的账号',
  `ACCOUNT_BALANCE` varchar(100) DEFAULT NULL COMMENT '余额',
  `BANK_ID` int(11) DEFAULT NULL COMMENT '银行的ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;

/*Data for the table `cgj_account_t` */

insert  into `cgj_account_t`(`ID`,`ACCOUNT_NAME`,`ACCOUNT_NUMBER`,`ACCOUNT_BALANCE`,`BANK_ID`) values (1001,'王超','8009-1001-1','100元',1),(1002,'陈叶','8009-1001-2','1000元',2);

/*Table structure for table `cgj_bank_t` */

DROP TABLE IF EXISTS `cgj_bank_t`;

CREATE TABLE `cgj_bank_t` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '银行ID',
  `BANK_NAME` varchar(100) DEFAULT NULL COMMENT '银行名称',
  `BANK_PHONE` varchar(50) DEFAULT NULL COMMENT '银行总机号码',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `cgj_bank_t` */

insert  into `cgj_bank_t`(`ID`,`BANK_NAME`,`BANK_PHONE`) values (1,'中国银行','80088'),(2,'中国农业银行','80066'),(3,'中国工商银行','80086'),(4,'中国农村银子','80011'),(5,'农村合作社','111158'),(10,'11','22');

/*Table structure for table `cgj_menu_t` */

DROP TABLE IF EXISTS `cgj_menu_t`;

CREATE TABLE `cgj_menu_t` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MENU_NAME` varchar(128) DEFAULT NULL COMMENT '菜单的名称',
  `URL` varchar(128) DEFAULT NULL COMMENT '点击左边的菜单名称。右边出现什么页面',
  `FATHER_ID` int(11) DEFAULT NULL COMMENT '体现层级关系  如果是根节点 那么FATHER_ID 为 null',
  `ICON` varchar(64) DEFAULT NULL COMMENT '菜单栏左侧的图标 如果为空。就会用默认的图标',
  `ORDER_NO` decimal(10,2) DEFAULT NULL COMMENT '排序号 从小到大 升序',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `cgj_menu_t` */

insert  into `cgj_menu_t`(`ID`,`MENU_NAME`,`URL`,`FATHER_ID`,`ICON`,`ORDER_NO`) values (1,'银行系统',NULL,NULL,NULL,'0.00'),(2,'银行管理','bank.jsp',1,'icon-add','2.00'),(7,'储户管理','account.jsp',1,NULL,'3.00');

/*Table structure for table `cgj_user_t` */

DROP TABLE IF EXISTS `cgj_user_t`;

CREATE TABLE `cgj_user_t` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工ID',
  `NAME` varchar(64) DEFAULT NULL COMMENT '员工姓名',
  `PHONE` varchar(64) DEFAULT NULL COMMENT '手机号码',
  `PASSWORD` varchar(64) NOT NULL COMMENT '密码',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10010015 DEFAULT CHARSET=utf8;

/*Data for the table `cgj_user_t` */

insert  into `cgj_user_t`(`ID`,`NAME`,`PHONE`,`PASSWORD`) values (10010011,'王超','18827195089','123456'),(10010012,'陈光进','15818245636','123456'),(10010013,NULL,'15818245630','fe10afdc3949fba59fabfbe56fe057ff20ff883e'),(10010014,NULL,'15818245630','fe10afdc3949fba59fabfbe56fe057ff20ff883e');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;