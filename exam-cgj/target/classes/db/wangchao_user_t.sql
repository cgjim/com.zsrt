/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50546
Source Host           : localhost:3306
Source Database       : exam-ssm-1609

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2016-12-16 00:36:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wangchao_user_t
-- ----------------------------
DROP TABLE IF EXISTS `wangchao_user_t`;
CREATE TABLE `wangchao_user_t` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工ID',
  `NAME` varchar(64) DEFAULT NULL COMMENT '员工姓名',
  `PHONE` varchar(64) DEFAULT NULL COMMENT '手机号码',
  `PASSWORD` varchar(64) NOT NULL COMMENT '密码',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1001018 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wangchao_user_t
-- ----------------------------
INSERT INTO `wangchao_user_t` VALUES ('10010011', '王超', '18827195089', '123456');
