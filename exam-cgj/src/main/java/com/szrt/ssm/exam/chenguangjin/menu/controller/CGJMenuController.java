package com.szrt.ssm.exam.chenguangjin.menu.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szrt.ssm.exam.chenguangjin.menu.dto.CGJEasyuiTreeNode;
import com.szrt.ssm.exam.chenguangjin.menu.entity.CGJMenuEntity;
import com.szrt.ssm.exam.chenguangjin.menu.service.CGJMenuService;




@Controller
@RequestMapping("/cgj/menu")
public class CGJMenuController {
	
	
	@Autowired
	private CGJMenuService service;
	
	
	
	@ResponseBody
	@RequestMapping("/init")
	public List<CGJEasyuiTreeNode> initTree(Integer id){
		//设计方法  当前台发送ajax请求的时候  给够把tree传回去
		List<CGJEasyuiTreeNode>  list  = new ArrayList<CGJEasyuiTreeNode>();
		if(null == id){
			CGJMenuEntity entity = service.findRootNode();
			
			
			CGJEasyuiTreeNode  node = new CGJEasyuiTreeNode();
			int counterChildren = service.checkLeaf(entity.getId());
			if(counterChildren>0){
				//说明还有孩子
				node.setState("closed");
				
			}
			node.setId(entity.getId());
			node.setText(entity.getMenuName());
			list.add(node);
		}else{
			List<CGJMenuEntity> menus =service.findChildren(id);
			for(CGJMenuEntity entity : menus){
				CGJEasyuiTreeNode  node = new CGJEasyuiTreeNode();
				
				Map<String,Object> attributes = new  HashMap<String,Object>();
				
				attributes.put("url", entity.getUrl());
				
				node.setAttributes(attributes);
				
				int counterChildren = service.checkLeaf(entity.getId());
				if(counterChildren>0){
					//说明还有孩子
					node.setState("closed");
				}
				node.setId(entity.getId());
				node.setText(entity.getMenuName());
				list.add(node);
			}
		}
		return list;
	}

}
