package com.szrt.ssm.exam.chenguangjin.user.entity;


public class CGJUserEntity {
	
	private Integer id;
	private String name;
	private String phone;  //手机号码
	private String password; //md5加密
	/**
	 * 使用手机号作为网站登录与注册对象的用户名，为数据库字段，用户可见不可修改,不能为空
	 */
	private String userPhone;
	/**
	 * 网站用户上传的头像地址，默认为自带头像
	 */
	private String userHeadPortrait="/exam-cgj/chenguangjin/img/default.jpg";
	
	public String getUserHeadPortrait() {
		return userHeadPortrait;
	}

	public void setUserHeadPortrait(String userHeadPortrait) {
		this.userHeadPortrait = userHeadPortrait;
	}
	//以下非数据库字段
	
	private String companyName;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	
	

}
