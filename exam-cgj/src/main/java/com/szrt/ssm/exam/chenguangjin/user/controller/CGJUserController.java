package com.szrt.ssm.exam.chenguangjin.user.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.szrt.ssm.exam.chenguangjin.user.entity.CGJUserEntity;
import com.szrt.ssm.exam.chenguangjin.user.service.CGJUserService;


@Controller
@RequestMapping("/cgj/user")
public class CGJUserController {
	
	/**
	 * @Resource JSR-250  导包
	 * @Reject   JSR-330  导包
	 */
	@Autowired 
	private  CGJUserService service;
	
	/**
	*采用spring提供的上传文件的方法
    */
   @RequestMapping("/loadFile")
   public String  springUpload(HttpServletRequest request) throws IllegalStateException, IOException
   {
        long  startTime=System.currentTimeMillis();
        String dataBaseUrl=null;
        //将当前上下文初始化给  CommonsMutipartResolver （多部分解析器）
       CommonsMultipartResolver multipartResolver=new CommonsMultipartResolver(
               request.getSession().getServletContext());
       //检查form中是否有enctype="multipart/form-data"
       if(multipartResolver.isMultipart(request))
       {	
           //将request变成多部分request
           MultipartHttpServletRequest multiRequest=(MultipartHttpServletRequest)request;
          //获取multiRequest 中所有的文件名
           Iterator<String> iter=multiRequest.getFileNames();
            
           while(iter.hasNext())
           {
               //一次遍历所有文件
               MultipartFile file=multiRequest.getFile(iter.next().toString());
               if(file!=null)
               {
            	   
            	   
            	   String xx = request.getServletContext().getRealPath("/");
            	   System.out.println(xx);
                   String path=xx+"chenguangjin/img/"+file.getOriginalFilename();
                   dataBaseUrl="/chenguangjin/img/"+file.getOriginalFilename();
                   System.out.println(dataBaseUrl);
                   
                   
                   //D:\javaTool\apache-tomcat-7.0.73\wtpwebapps\mvn-esayui-ssm-1609\chenguangjin\img
                   
                   //上传
                   file.transferTo(new File(path));
               }
                
           }
          
       }
       long  endTime=System.currentTimeMillis();
       if(null!=dataBaseUrl){
    	   //如果存入数据库中的路径字符串不为空，那么实例化一个用户对象
    	   CGJUserEntity entity=new CGJUserEntity();
    	   //将当前session会话中的userPhone值取出，与可存于数据库的地址一起设置到用户对象中
           String userPhonne = (String) request.getSession().getAttribute("userPhone");
           entity.setUserPhone(userPhonne);
           entity.setUserHeadPortrait(dataBaseUrl);
           //调用service的更新方法，将数据库中的图片地址更新
           service.updateUserHeadPortrait(entity);
           //设置session会话的图片地址为新的图片地址，以带刷新时更新图片地址
           request.getSession().setAttribute("userHeadPortrait", dataBaseUrl);
       }
       System.out.println("方法三的运行时间："+String.valueOf(endTime-startTime)+"ms");
       return "/chenguangjin/user/success"; 
   }
	
	/**
	 * 注册
	 */
	@RequestMapping(value="/userRegister")
	@ResponseBody
	public Map<String,Object>  userRegister(CGJUserEntity entity){
		Map<String,Object> reuslt = new HashMap<String,Object>();
		service.userRegister(entity,reuslt);
		return reuslt;
	}
	
	/**
	 * 登陆
	 */
	@RequestMapping(value="/login")
	@ResponseBody
	public Map<String,Object> login(CGJUserEntity entity){
		//String[] idss =ids.split(",");
		Map<String,Object> reuslt = new HashMap<String,Object>();
		service.login(entity, reuslt);
		return reuslt;
	}
	
}
