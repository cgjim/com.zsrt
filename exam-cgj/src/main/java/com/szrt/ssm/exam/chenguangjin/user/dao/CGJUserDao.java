package com.szrt.ssm.exam.chenguangjin.user.dao;


import com.szrt.ssm.exam.chenguangjin.user.entity.CGJUserEntity;


public interface CGJUserDao {
	
	/**
	 *用户登陆
	 * @param entity
	 * @return
	 */
	int login(CGJUserEntity entity);	
	
	/**
	 * 用户注册
	 * @param entity
	 */
	int userRegister(CGJUserEntity entity);
	int userCount(CGJUserEntity entity);
	
}
