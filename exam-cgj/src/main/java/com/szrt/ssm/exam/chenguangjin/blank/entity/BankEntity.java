package com.szrt.ssm.exam.chenguangjin.blank.entity;

/**
 * 银行实体类
 * @author Administrator
 *
 */
public class BankEntity {

	private Integer id;
	private String bankName;
	private String bankPhone;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankPhone() {
		return bankPhone;
	}
	public void setBankPhone(String bankPhone) {
		this.bankPhone = bankPhone;
	}
	
}
