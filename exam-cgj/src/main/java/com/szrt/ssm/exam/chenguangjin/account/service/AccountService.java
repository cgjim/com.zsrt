package com.szrt.ssm.exam.chenguangjin.account.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.szrt.ssm.exam.chenguangjin.account.dao.AccountDao;
import com.szrt.ssm.exam.chenguangjin.account.entity.AccountEntity;
import com.szrt.ssm.exam.chenguangjin.blank.entity.BankEntity;
import com.szrt.ssm.exam.chenguangjin.common.MessageDTO;


@Service
public class AccountService {
	
	@Autowired
	private AccountDao dao;
	
	/**
	 * 分页显示所有
	 * @param fenye
	 * @param reuslt
	 */
	public void findAll(Map<String, Object> fenye, Map<String, Object> reuslt) {
		try{
			//显示多少行
			List<AccountEntity> rows=dao.findAll(fenye);
			//共有几条数据
			int total =dao.counterEntity(fenye);
			reuslt.put("rows", rows);
			reuslt.put("total", total);
			MessageDTO.putSuccessMessage(reuslt);
		}catch(Exception e){
			MessageDTO.putErrorMessage(reuslt);
			e.printStackTrace();
		}
	}
	
	/**
	 * 添加方法， 增加事务 回滚
	 * @param entity
	 * @param result
	 */
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=RuntimeException.class)
	public void add(AccountEntity entity, Map<String, Object> result) {
		try{
			dao.add(entity);
			MessageDTO.putSuccessMessage(result);
		}catch(Exception e){
			MessageDTO.putFailureMessage(result);
			e.printStackTrace();
			throw new RuntimeException();
		}
		
	}
	
	public List<BankEntity> findAllKeyAndValue() {
			return dao.findAllKeyAndValue();
	}
	
	/**
	 * 删除方法
	 * @param ids
	 * @param reuslt
	 */
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=RuntimeException.class)
	public void deleteByIds(List<Integer> ids, Map<String, Object> reuslt) {
		try{
			//影响的记录的行数
			dao.deleteByIds(ids);
			
			MessageDTO.putSuccessMessage(reuslt);
		}catch(Exception e){
			MessageDTO.putErrorMessage(reuslt);
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	
	@Transactional(propagation =Propagation.REQUIRED,rollbackFor=RuntimeException.class)
	public void update(AccountEntity entity, Map<String, Object> reuslt) {
		try{
			//影响的记录的行数
			dao.update(entity);
			MessageDTO.putSuccessMessage(reuslt);
		}catch(Exception e){
			MessageDTO.putErrorMessage(reuslt);
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public AccountEntity findById(Integer id) {
		AccountEntity entity  = null;
		try{
				
			//影响的记录的行数
			entity =dao.findById(id);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return entity;
	}

	public List<AccountEntity> findProfit() {
		 
		return dao.findProfit();
	}

	
	
}
