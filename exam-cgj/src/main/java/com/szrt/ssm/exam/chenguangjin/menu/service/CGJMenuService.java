package com.szrt.ssm.exam.chenguangjin.menu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szrt.ssm.exam.chenguangjin.menu.dao.CGJMenuDao;
import com.szrt.ssm.exam.chenguangjin.menu.entity.CGJMenuEntity;




@Service
public class CGJMenuService  {
	
	@Autowired
	private CGJMenuDao dao;

	/**
	 * 找到根节点
	 */
	public CGJMenuEntity findRootNode() {
		return dao.findRootNode();
	}

	/**
	 * 判断当前节点是不是叶子节点  如果返回则为0  说明是叶子节点  否则说明当前节点下面有子节点
	 * expandAll  再次调用  直到 这个节点是叶子节点为止
	 * @param id
	 * @return
	 */
	public List<CGJMenuEntity> findChildren(Integer id) {
		return dao.findChildren(id);
	}

	/**
	 * 找到当前节点下面的子节点
	 * @param id
	 * @return
	 */
	public int checkLeaf(int id) {
		return dao.checkLeaf(id);
	}

}
