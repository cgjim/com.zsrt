package com.szrt.ssm.exam.chenguangjin.user.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.szrt.ssm.exam.chenguangjin.common.MessageDTO;
import com.szrt.ssm.exam.chenguangjin.user.dao.CGJUserDao;
import com.szrt.ssm.exam.chenguangjin.user.entity.CGJUserEntity;
import com.szrt.ssm.exam.chenguangjin.util.MD5;



@Service
public class CGJUserService {
	
	@Autowired
	private CGJUserDao dao;
	
	/**
	 * 登陆方法  1则表示登陆成功  否则登陆失败
	 * @param entity
	 * @param reuslt
	 */
	
	public void login(CGJUserEntity entity,Map<String, Object> reuslt){
		try{
			
			String password=MD5.toMD5(entity.getPassword());
			entity.setPassword(password);
			int counter = dao.login(entity);
			if(counter==1){
				MessageDTO.putSuccessMessage(reuslt);
			}else{
				MessageDTO.putErrorMessage(reuslt);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	/**
	 * 用户注册
	 * @param entity
	 * @param reuslt 
	 */
	@Transactional(propagation =Propagation.REQUIRED,rollbackFor=RuntimeException.class)
	public void userRegister(CGJUserEntity entity, Map<String, Object> reuslt) {
		try{
			//密码以MD5加密
			String password=MD5.toMD5(entity.getPassword());
			entity.setPassword(password);
			int userAmount=dao.userCount(entity);
			int counter = dao.userRegister(entity);
			//判断有数据进来
			if(counter==1){
				//判断用户名是否重复，0代表不重复
				if(userAmount==0){
					MessageDTO.putSuccessMessage(reuslt);
				}else{
					MessageDTO.putErrorMessage(reuslt);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}
	}


	public void updateUserHeadPortrait(CGJUserEntity entity) {
		// TODO Auto-generated method stub
		
	}
}
