package com.szrt.ssm.exam.chenguangjin.account.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szrt.ssm.exam.chenguangjin.account.entity.AccountEntity;
import com.szrt.ssm.exam.chenguangjin.account.service.AccountService;
import com.szrt.ssm.exam.chenguangjin.blank.entity.BankEntity;


@Controller
@RequestMapping("/cgj/account")
public class AccountController {
	
	@Autowired
	private AccountService service;
	
	@RequestMapping(value="/findProfit")
	@ResponseBody
	public List<AccountEntity> findProfit(){
		List<AccountEntity> profitList=new ArrayList<AccountEntity>();
		profitList=service.findProfit();
		return profitList;
	}
	
	@RequestMapping(value="/findById")
	@ResponseBody
	public AccountEntity findById(Integer id){
		
		AccountEntity entity =service.findById(id);
		return entity;
	}
	
	@RequestMapping(value="/update")
	@ResponseBody
	public Map<String,Object> update(AccountEntity entity){
		Map<String,Object> reuslt = new HashMap<String,Object>();
		
		service.update(entity,reuslt);
		return reuslt;
	}
	
	@RequestMapping(value="/deleteByIds")
	@ResponseBody
	public Map<String,Object> deleteByIds(@RequestBody List<Integer> ids){
		
		//String[] idss =ids.split(",");
		Map<String,Object> reuslt = new HashMap<String,Object>();
		
		service.deleteByIds(ids,reuslt);
		return reuslt;
	}
	
	@RequestMapping(value="/findAllKeyAndValue")
	@ResponseBody
	public List<BankEntity> findAllKeyAndValue(){
		
		List<BankEntity> list = new ArrayList<BankEntity>();
		
		list=service.findAllKeyAndValue();
		return list;
	}
	
	@ResponseBody
	@RequestMapping("/add")
	public Map<String,Object> add(AccountEntity entity){
		Map<String,Object> result=new HashMap<String,Object>();
		service.add(entity,result);
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/findAll")
	public Map<String,Object> findAll(Integer page,Integer rows,String accountName){
		System.out.println("BBBB");
		System.err.println("bbbbbbbb===="+accountName);
		Integer pageNumber = page;
		Integer pageSize   = rows;
		
		Integer before =pageSize*(pageNumber-1) ;
		Integer after  =pageSize;
		
		Map<String, Object> fenye = new HashMap<String,Object>();
		fenye.put("before", before);
		fenye.put("after", after);
		
		if(null != accountName){
			fenye.put("accountName", "%"+accountName+"%");
		}
		Map<String,Object> reuslt = new HashMap<String,Object>();
		service.findAll(fenye,reuslt);
		return reuslt;
	}
}
