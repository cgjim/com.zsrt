package com.szrt.ssm.exam.chenguangjin.menu.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 专用用来 把 MenuEntity 转换 匹配EasyUi需要的数据格式
 * @author Administrator
 *
 */
public class CGJEasyuiTreeNode  implements Serializable {
	
	private static final long serialVersionUID = -2327841811030993903L;

	/**
	 * 节点ID，对加载远程数据很重要。
	 */
	private Integer id;
	
	/**
	 * 显示节点文本。
	 */
	private String  text;
	
	/**
	 * 节点状态，'open' 或 'closed'，默认：'open'。如果为'closed'的时候，将不自动展开该节点。
	 */
	private String state="open";
	
	/**
	 * 表示该节点是否被选中
	 */
	private boolean checked;
	
	/**
	 * 被添加到节点的自定义属性  url
	 */
	private Map<String,Object> attributes = new  HashMap<String,Object>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}
	
	
	
}
