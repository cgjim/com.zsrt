package com.szrt.ssm.exam.chenguangjin.blank.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szrt.ssm.exam.chenguangjin.blank.entity.BankEntity;
import com.szrt.ssm.exam.chenguangjin.blank.service.BankService;

@Controller
@RequestMapping("cgj/bank")
public class BankController {
	
	@Autowired
	private BankService service;
	
	/**
	 * 显示所有银行信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping("findAllPage")
	public  Map<String,Object> findAllPage(Integer page,Integer rows,String bankName){
		Integer pageNumber = page;
		Integer pageSize   = rows;
		
		Integer before =pageSize*(pageNumber-1) ;
		Integer after  =pageSize;

		//用来封装分页的数据  并且把这个数据传递给servcie
		Map<String,Object> fenye=new HashMap<String,Object>();
		
		fenye.put("before", before);
		fenye.put("after", after);
		
		if(null != bankName){
			fenye.put("bankName", "%"+bankName+"%");
		}
		System.out.println(pageNumber + " : "+ pageSize);
		
		//用来接收Service执行查询之后返回的结果
		Map<String,Object> reuslt = new HashMap<String,Object>();
		service.findAllPage(fenye,reuslt);
		return reuslt;
	}
	
	/**
	 * 添加 
	 * @param entity
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Map<String,Object> add(BankEntity entity){
		Map<String,Object> result=new HashMap<String,Object>();
		service.add(entity,result);
		return result;
	}
	
	/**
	 * 通过ID删除 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value="/deleteByIds")
	@ResponseBody
	public Map<String,Object> deleteByIds(@RequestBody List<Integer> ids){
		
		Map<String,Object> reuslt = new HashMap<String,Object>();
		
		service.deleteByIds(ids,reuslt);
		return reuslt;
	}
	
	@RequestMapping(value="/update")
	@ResponseBody
	public Map<String,Object> update(BankEntity entity){
		Map<String,Object> reuslt = new HashMap<String,Object>();
		
		service.update(entity,reuslt);
		return reuslt;
	}
	
	
	@RequestMapping(value="/findById")
	@ResponseBody
	public BankEntity findById(Integer id){
		
		BankEntity entity =service.findById(id);
		return entity;
	}
}
