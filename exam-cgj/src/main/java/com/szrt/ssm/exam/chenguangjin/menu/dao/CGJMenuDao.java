package com.szrt.ssm.exam.chenguangjin.menu.dao;

import java.util.List;

import com.szrt.ssm.exam.chenguangjin.menu.entity.CGJMenuEntity;


public interface CGJMenuDao {
	
	
	/**
	 * 找到根节点
	 */
	CGJMenuEntity findRootNode();
	
	
	
	/**
	 * 判断当前节点是不是叶子节点  如果返回则为0  说明是叶子节点  否则说明当前节点下面有子节点
	 * expandAll  再次调用  直到 这个节点是叶子节点为止
	 * @param id
	 * @return
	 */
	int checkLeaf(int id);
	
	
	/**
	 * 找到当前节点下面的子节点
	 * @param id
	 * @return
	 */
	List<CGJMenuEntity> findChildren(Integer id);

}
