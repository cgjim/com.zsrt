package com.szrt.ssm.exam.chenguangjin.account.dao;

import java.util.List;
import java.util.Map;

import com.szrt.ssm.exam.chenguangjin.account.entity.AccountEntity;
import com.szrt.ssm.exam.chenguangjin.blank.entity.BankEntity;



public interface AccountDao {

	/**
	 * 分布查询所有信托产品
	 * @param fenye
	 * @return
	 */
	List<AccountEntity> findAll(Map<String, Object> fenye);
	
	/**
	 * 模糊查询或者普通查询中  用来 显示多少页
	 * @param fenye
	 * @return
	 */
	int counterEntity(Map<String, Object> fenye);

	/**
	 * 添加
	 * @param entity
	 */
	void add(AccountEntity entity);

	/**
	 * 用于转化ID和value
	 * @return
	 */
	List<BankEntity> findAllKeyAndValue();

	/**
	 * 删除
	 * @param ids
	 */
	void deleteByIds(List<Integer> ids);

	/**
	 * 修改
	 * @param entity
	 */
	void update(AccountEntity entity);

	/**
	 * 查找ID  用于更新时 跳到保存弹窗
	 * @param id
	 * @return
	 */
	AccountEntity findById(Integer id);



	/**
	 * 查询收益数据表，显示到前台
	 * @return
	 */
	List<AccountEntity> findProfit();
}
