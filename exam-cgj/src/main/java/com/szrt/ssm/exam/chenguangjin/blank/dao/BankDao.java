package com.szrt.ssm.exam.chenguangjin.blank.dao;

import java.util.List;
import java.util.Map;

import com.szrt.ssm.exam.chenguangjin.blank.entity.BankEntity;

public interface BankDao {

	List<BankEntity> findAllPage(Map<String, Object> fenye);

	int counterEntity(Map<String, Object> fenye);

	void add(BankEntity entity);

	void deleteByIds(List<Integer> ids);

	void update(BankEntity entity);

	BankEntity findById(Integer id);

}
