package com.szrt.ssm.exam.chenguangjin.menu.entity;


/**
 * 西边的菜单
 * @author Administrator
 *
 */
public class CGJMenuEntity {
	
	private Integer id;
	private String  menuName;
	private String  url;
	private Integer fatherId;
	private String  icon;
	private Double  orderNo;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getFatherId() {
		return fatherId;
	}
	public void setFatherId(Integer fatherId) {
		this.fatherId = fatherId;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Double getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Double orderNo) {
		this.orderNo = orderNo;
	}
	
	

}
