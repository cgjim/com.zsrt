<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎注册</title>																	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/chenguangjin/css/cgj_login.css "/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/chenguangjin/js/jquery-1.7.2.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/chenguangjin/js/cgj_register.js" ></script>
	<jsp:include page="/common/common.jsp"></jsp:include>
</head>
<!-- http://localhost:8080/exam-cgj/chenguangjin/register.jsp -->
<body>
	<div class="regContainer">
		 <form id="ff"   method="post"  >
				<div id="register">
					<h1>欢迎注册</h1>
					<!--用户名和密码-->
					<img src="img/user.jpg" class="usernameImg"  />
					<div id="usernameClass">
						<input type="text" name="phone" class="textClass" placeholder="请输入手机号码"  />
						<div><span id="usernameSpan"></span></div>
					</div>
					<img src="img/password.jpg" class="passwordImg"  />
					<div id="passwordClass">
						<input type="password" name="password" class="textClass" placeholder="请输入密码(6-20位)" />
						<div><span id="passwordSpan"></span></div>
					</div>
					<img src="img/password.jpg" class="passwordImg2"  />
					<div id="passwordClass2">
						<input type="password" name="password2" class="textClass" placeholder="请再次输入密码" />
						<div><span id="password2Span"></span></div>
					</div>
					
					<!--注册按钮-->
					<div class="buttonClass">
						<input type="button" name="register" id="registerButton2"  value="立即注册" />
					</div>
				</div>	
			</form>				
		</div>
</body>
</html>