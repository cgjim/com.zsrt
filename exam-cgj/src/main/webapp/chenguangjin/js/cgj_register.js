$(function(){
	//input 的名字必须要和数据库的字段相同  也就是实体
	var usernameInput=$('input[name="phone"]');
	var passwordInput=$('input[name="password"]');
	var password2Input=$('input[name="password2"]');
	var registerBut=$('#registerButton2');
	var userFlag;
	var passFlag;
	var pass2Flag;
	
	/* 鼠标移动事件  验证用户名！*/
	usernameInput.blur(function(){
		userFlag=false;
		var usernameVal=usernameInput.val();
		var uspan=$('#usernameSpan');
		var re=/^1[3|5|6|7|8][0-9]{9}$/;
		var pass=re.test(usernameVal);
		if(pass){
			uspan.css({
				'color':'green',
				'font-weight':'bold',
			})
			uspan.html('√');
			userFlag=true;
		}else{
			uspan.css({
				'color':'red',
			})
			uspan.html('手机号码有误，请重新输入');
		}
		return userFlag;
	});
	
	/* 验证密码！ 鼠标移动事件  */
	passwordInput.blur(function(){
		passFlag=false;
		var passwordVal=passwordInput.val();
		var pspan=$('#passwordSpan');
		var re2=/^[0-9a-zA-Z]{6,20}$/;
		var pp=re2.test(passwordVal);
		if(pp){
			pspan.css({
				'color':'green',
				'font-weight':'bold',
			})
			pspan.html('√');
		    passFlag=true;
		}else{
			pspan.css({
				'color':'red',
			})
			pspan.html('密码必须为6-20位任意数字或字符');
			passFlag=false;
		}
		return passFlag;
	});
	
	/* 鼠标移动事件 再次验证密码！*/
	password2Input.blur(function(){
		pass2Flag=false;
		var passwordVal=passwordInput.val();
		var  passw2Val=password2Input.val();
		var pssw2Span=$('#password2Span');
		/*console.log(passwordVal+'=='+passw2Val);*/
		if(passFlag){
			if(passwordVal==passw2Val){
				pssw2Span.css({
					'color':'green',
				})
				pssw2Span.html('两次密码一致');
				pass2Flag=true;
			}else{
				pssw2Span.css({
					'color':'red',
				})
				pssw2Span.html('两密码不一致，请重新输入');
			}
		}
		return pass2Flag;
	});
	
	/*注册按钮  以上三个输入框都通过才可以点击 注册 并把 用户名和密码的值传到后台*/
	$('#registerButton2').click(function(){
		if(userFlag && passFlag && pass2Flag){
			/*$('#formId').form('submit',{*/
			$('#ff').form('submit',{
				url:'http://localhost:8080/exam-cgj/cgj/user/userRegister',
				data:{
					'phone':usernameInput.val(),
					'password':passwordInput.val(),
				},
				success:function(data){
					var jsonData = eval('('+data+')');
			    	var msg = jsonData.data.message;
			    	var flag = jsonData.data.flag;
			    	if(1==flag){
			    		var c=confirm('恭喜！注册成功，跳转到登陆页面?');
			    		if(c){
			    			window.location.href='http://localhost:8080/exam-cgj/chenguangjin/login.jsp';
			    		}else{
			    			//否则留在当前页面
			    		}
			    	}else{
			    		//后台判断 数据库中的用户名是否相同，相同则提示用户名重复了
			    		alert("抱歉！该用户名已经存在，请重新输入！");
			    	}
				}
			});
		}
			
	})
	//注册按钮点击事件结束
});