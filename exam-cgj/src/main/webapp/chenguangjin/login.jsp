<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎登陆</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/chenguangjin/css/cgj_login.css "/>
	<jsp:include page="/common/common.jsp"></jsp:include>
</head>
<!-- http://localhost:8080/exam-cgj/chenguangjin/login.jsp -->
<body>
<div class="container">
	<form id="ff"  method="post">
		<div id="login">
			<h1>欢迎登陆</h1>
			<!--用户名和密码-->
			<!-- <img src="img/user.jpg" class="usernameImg"  /> -->
			<div id="usernameClass">
				<input type="text" name="phone" class="textClass easyui-textbox" data-options="iconCls:'icon-man'" value="15818245636" placeholder="请输入手机号码"  />
			</div>
			<img src="img/password.jpg" class="passwordImg"  />
			<div id="passwordClass">
				<input type="password" name="password" class="textClass" value="123456" placeholder="请输入密码" />
			</div>
			
			<!--登陆和注册-->
			<div class="buttonClass">
				<input type="button" name="login" id="loginButton" value="登陆"  />
				<a href="register.jsp"><input type="button" name="register" id="registerButton"  value="注册" /></a>
			</div>
	    </div>
	</form>
</div>
 <script type="text/javascript">
	 $(function(){
		 $('#loginButton').click(function(){
				var phone =$('input[name=phone]').val();	
				var password =$('input[name=password]').val();
				//登录开始
				$('#ff').form('submit',{
					url:'${pageContext.request.contextPath}/cgj/user/login',
					//传给后台的数据
					data:{
						'phone':phone,
						'password':password,
					},
					success:function(data){
						//alert(jsonData);
						var jsonData = eval('('+data+')');
				    	var msg = jsonData.data.message;
				    	var flag = jsonData.data.flag;
				        console.log(flag+":"+msg);  
				        if(1==flag){
				        	//setTimeout("window.location.href='${pageContext.request.contextPath}/pages/user/list.jsp'",1000);
				        	window.location.href='${pageContext.request.contextPath}/chenguangjin/layout/layout.jsp';
				        }else{
				        	alert("用户名或密码不正确");
				        }
					}
			//登录结束			
				 });
		 });
	 });
	 
	 </script>
</body>
</html>