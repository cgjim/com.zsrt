<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>layout</title>
		<!-- 引入easyui所需要的js和css文件 -->
		<jsp:include page="/common/common.jsp"></jsp:include>
		
		<!-- http://localhost:8080/exam-cgj/chenguangjin/layout/layout.jsp -->
	</head>
	<body class="easyui-layout" >
		<div     data-options="region:'north',title:'北部',href:'top.html'" style="height:100px;"></div>   
	    <div	 data-options="region:'west',title:'西部',split:true,href:'menuDyn.jsp'" style="width:150px;"></div>   
	    <div	 data-options="region:'south',title:'南部'" style="height:120px;" align="center">
	    	<h6>增值电信业务经营许可证：浙B2-20080224  信息网络传播视听节目许可证：1109364号</h6>
	    </div>   
	    <div data-options="region:'center',title:'主体内容',href:'center.jsp'" style="padding:5px;background:#eee;"></div>   
		
	</body>
</html>