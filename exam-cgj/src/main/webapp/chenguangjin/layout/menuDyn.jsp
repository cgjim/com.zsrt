<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>layout</title>
		
		<jsp:include page="../../common/common.jsp"></jsp:include>
		
		<!-- http://localhost:8080/easyui1607/demo/menuDyn.jsp -->
		<script type="text/javascript">
		$(function(){
			$('#mytree').tree({
				url:'${pageContext.request.contextPath}/cgj/menu/init',
				lines:true,
				
				/* //只要后台有数据返回就会调用这个方法
				onLoadSuccess:function(node,data){
					//data data是后台返回的数据对象 EasyuiTreeNode
					var t = $(this);
					console.log('data='+data);
					console.log('node='+node)
					if(data){
						$(data).each(function(index,d){
							if(this.state=='closed'){
								//t.tree('expandAll');
							}
						});
					}
					
					
				}, //end of onLoadSuccess */
				
				
				//当点击左边的菜单栏的时候触发这个方法
				onClick: function (node) {
                	console.log(node);
                    // 增加1个新的页签   
                	if ($('#scottTabs').tabs('exists', node.text)){
                		$('#scottTabs').tabs('select', node.text);
                	}else{
                		$('#scottTabs').tabs('add', {
                        	//页签的标题
                            title:  node.text,
                            //点击菜单栏之后连接到哪里去
                            href: node.attributes.url,
                            //页签是否可以关闭
                            closable: true,

                        });  
                	}
                }//end of onClick
				
				
			});
		});
		</script>
	</head>
	<body>
		<ul id="mytree">   
		</ul>  
	</body>
	
	
	
</html>