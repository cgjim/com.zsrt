<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>信托的产品</title>
<jsp:include page="../../common/common.jsp"></jsp:include>
</head>
<!-- http://localhost:8080/mvn-esayui-ssm-1609/chenguangjin/layout/xtProduct.jsp -->
<body>
	<table id="accountTable"></table>
	
	<div id="accountToolBar">
		<input id="searchBox" class="easyui-text" name="accountName" />
		<a class="easyui-linkbutton" iconCls="icon-search" plain="ture" onclick="searchName();">查询</a>
					
		<br/>
		<a class="easyui-linkbutton" iconCls="icon-add" onclick="add('产品新增');" >增加</a>
		<a class="easyui-linkbutton" iconCls="icon-remove"  onclick="deleteByIds();" >删除</a>
		<a class="easyui-linkbutton" iconCls="icon-edit" onclick="update();" >修改</a>
	</div>
	
	<!-- 新增和修改所使用的div 开始 -->
	<div  id="accountDialog" style="display: none" >
		<form id="accountForm" method="post">
			<table >
				<tr>
					<td>产品名称：</td>
					<td><input class="easyui-validatebox" data-options="required:true"  name="accountName" /></td>
					
					<!-- 有些东西 我们后台需要 但是前台不显示的处理方法....  hidden -->
					<input type="hidden" name="id" />
					<!-- 有些东西 我们后台需要 但是前台不显示的处理方法....  hidden -->
				</tr>
				<tr>
					<td >最低认购金额：</td>
					<td><input class="easyui-validatebox" data-options="required:true" name="accountNum" /></td>
				</tr>
				
				<tr>
					<td>收益：</td>
					<td>
					<input class="easyui-validatebox" data-options="required:true" name="accountBalance" />
					</td>
							
					</tr>
				<tr>
					<td>公司名称：</td>
					<td>
						<input id="bankID" 
					        	class="easyui-combobox" 
					        	name="bankID" 
					        	style="width:160px;" 
					        	data-options="editable:false,valueField:'bankID', textField:'bankName'" /> 
					</td>
				</tr>
				
				<tr>
					<td><a class="easyui-linkbutton" iconCls="icon-ok" onclick="save();" >保存</a></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 新增和修改所使用的div 结束 -->
<script type="text/javascript">
	
	var addOrUpdate = 'add';
	
	//删除方法开始
	function deleteByIds(){
		var url   ='${pageContext.request.contextPath}/cgj/account/deleteByIds';
		//1 获得选中的id 
		var rows = $("#accountTable").datagrid('getSelections');
		// [10,20,30]
		var ids=[];
		
		$(rows).each(function(index,data){
			
			//1 拿到你要删除的id
			var id  = data.id;
			//2把id放到数组中去
			ids.push(id);
		});
	
		//2提示用户是否确定删除
		$.messager.confirm('确认','您确认想要删除记录吗？',function(flag){ 
			
			//如果用户选择是  flag=true  否则就表示false
		    if (flag){    
		      	//3把ids传递给后台   后台会重新查询1次  自动更新 重载数据
		      	
		      	$.ajax({
		      		url:url,
		      		type:'post',
		      		contentType:'application/json',
		      		//[10,20,30]
		      		data: JSON.stringify(ids),
		      		dataType:'json',
		      		success:function(jsonData){
		      			if(jsonData.data.flag=='1'){
							//1 说明后台都已经输出数据成功
		      				$.messager.show({
								title:'提示', msg:jsonData.data.message, 	timeout:2000
							});
		      				//2 重新请求后台 再次加载数据
		      				$('#accountTable').datagrid('reload');
						}
		      		}
		      	});
		    }    
		});  
	}
	//删除方法结束

//update方法开始
function update(){
	//2给对话框里赋值  拿到 原来值
	//2.1.1 怎么拿到id
	//返回所有被选中的行，当没有记录被选中的时候将返回一个空数组。
	var rows = $("#accountTable").datagrid('getSelections');
	var length = rows.length ;
	if(length == 1){
		//1 给我弹出1个干净的对话框
		add('产品修改');
		var row = rows[0];
		var id = row.id;
		//$.messager.alert('测试',id);   
		//2.1 把id传给后台发一个请求  拿到对象  把后台传过来的对象 分别给我们的文本框            
		$("#accountForm").form('load','${pageContext.request.contextPath}/cgj/account/findById?id='+id);
		//在此处明确知道是什么更新操作
		addOrUpdate = 'update';
		//3提交数据到后台
		//4前台接收数据
	}else{
		$.messager.alert('警告','只能选择1行修改...');   
		return ;
	}
}
//update方法结束
		
//增加方法开始 		
	function add(title){
	//每次弹框 清空 表
	$('#accountForm').form('clear');
		//点击“添加” 对话框就会弹出
		$("#accountDialog").dialog({
			//1 让隐藏的东西显示
			closed:false,
			width:300,
			height:380,
			title:title,
			//模块化 ：  让其他的东西都不能操作
			modal:true,
		});
		
		$.ajax({
			url: '${pageContext.request.contextPath}/cgj/account/findAllKeyAndValue',   
	        type:'post',  
	        success:function(data){  
	            var themecombo2 =[{ 'bankName':'请选择','bankID':''}];  
	                for(var i=0;i<data.length;i++){  
	                themecombo2.push({"bankName":data[i].bankName,"bankID":data[i].bankID});  
	                }  
	            /* $("#bankID").combobox("loadData", themecombo2); */  
	            $("#bankID").combobox({
	            	prompt:'输入首关键字自动检索',  
	        	    required:false,  
	        	    url:'${pageContext.request.contextPath}/cgj/account/findAllKeyAndValue',  
	        	    editable:true,  
	        	    hasDownArrow:true,  
	        	    filter: function(q, row){  
	        	        var opts = $(this).combobox('options');  
	        	        return row[opts.textField].indexOf(q) == 0;  
	        	    }  
	            });  
 	         }//成功data结束
			
		});
		/* //收益下拉列表的ajax    
		 $.ajax({
			url: '${pageContext.request.contextPath}/cgj/xt/findaccountBalance',   
	        type:'post',  
	        success:function(data){  
	            var themecombo3 =[{ 'accountBalance':'请选择','accountBalance':''}];  
	                for(var i=0;i<data.length;i++){  
	                	themecombo3.push({"accountBalance":data[i].accountBalance,"accountBalance":data[i].accountBalance});  
	                }  
	            $("#accountBalanceId").combobox("loadData", themecombo3);  
	         }  
		}); 
		 */
	};
	//add方法结束	
	
	//模糊查询开始 
	function searchName(){
		var accountName=$('#searchBox').val();
		if(null != accountName && ""!=accountName.trim()){
			$('#accountTable').datagrid('load',{
				'accountName':accountName
			});
		}
	};


//save保存方法开始
function save(){
	var myUrl ='';
	if(addOrUpdate == 'update'){
		myUrl='update';
	}else{
		myUrl='add';
	}
	//调用form 的 submit方法....
	$('#accountForm').form('submit',{
		url:'${pageContext.request.contextPath}/cgj/account/'+myUrl,
		//把数据提交到后台之前做数据校验。如果校验不通过 返回的是false。不提交表单。
		//只有返回值为true的时候才提交表单到后台
		success:function(jsonData){
			jsonData = eval("("+jsonData+")");
			
			var flag = jsonData.data.flag;
			var message = jsonData.data.message;
			console.log(flag);
			console.log(message);		
			//1 关闭对话框
			if(flag=='1'){
				//说明后台都已经插入数据成功 ，就关闭对话框
				$("#accountDialog").dialog('close');
				addOrUpdate ='add';
			}
			
			//2在右下角给出提示
			$.messager.show({
				title:'提示', msg:message, 	timeout:2000
			});
			
			//3 重新请求后台 再次加载数据 列表又一次更新
			$('#accountTable').datagrid('reload');
		}
	});
}
//save保存方法 结束
		

	$(function(){
			$('#accountTable').datagrid({
				title:'信托产品列表',
				width:500,
				height:300,
				
		        //增加分页逻辑的代码 begin
		        pageSize:3,
		        pagination:true,
		        pageList:[3,6,9,12],
		        //增加分页逻辑的代码 end
				
				//表示不换行
				nowrap:false,
				
				//工具栏
				toolbar: '#accountToolBar',

				fitColumns:true,
				url:'${pageContext.request.contextPath}/cgj/account/findAll',
				
				//表示表格的列  重点： 这里要用 二维数组： [[]] 兼容复杂的表格
				columns:[[{
					field:'',
					title:'',
					//可以使得 id列隐藏。然后如果做操作会自动传递隐藏的id  全选
					checkbox:true,
				},{
					field:'accountName',
					title:'储户名字',
					width:100,
					//sortable:true
				},{
					field:'accountNum',
					title:'储户账号',
					width:80,
					align:'right',
					//sortable:true
				},{
					field:'accountBalance',
					title:'余额',
					width:80,
					align:'right',
					//sortable:true
				},{
					field:'bankName',
					title:'银行名称',
					width:80,
					align:'right',
					//sortable:true
				}
				]],
			}); //end of datagrid
		
		});
</script>
</body>
</html>