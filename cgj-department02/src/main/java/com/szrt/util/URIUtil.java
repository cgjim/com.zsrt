package com.szrt.util;

import javax.servlet.http.HttpServletRequest;
/**
 * 
 * 怎么才能取到不同的result
 * @author Administrator
 *
 */
public class URIUtil {
	public static String getURI(String uri){
		int last = uri.lastIndexOf("/");
		String result = uri.substring(last+1);
		System.out.println(result);
		return result;
	}
	
	public static String getURI(HttpServletRequest request){
		//重点理解： getRequestURI 得到url的一部分 
		String uri= request.getRequestURI();
		int last = uri.lastIndexOf("/");
		String result = uri.substring(last+1,uri.length());
		return result;
	}
}
