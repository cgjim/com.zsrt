package com.szrt.dapartment.service;

import com.szrt.dapartment.enity.DepartmentEntity;

public interface DepartmentService {
	int add(DepartmentEntity entity) throws Exception;
	
	
	int delete(Integer departmentCode);
}
