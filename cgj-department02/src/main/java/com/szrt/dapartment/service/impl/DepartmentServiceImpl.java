package com.szrt.dapartment.service.impl;

import java.util.Set;

import com.szrt.dapartment.dao.DepartmentDao;
import com.szrt.dapartment.dao.impl.DepartmentDaoImpl;
import com.szrt.dapartment.enity.DepartmentEntity;
import com.szrt.dapartment.service.DepartmentService;

public class DepartmentServiceImpl implements DepartmentService {
	private DepartmentDao dao = new DepartmentDaoImpl();
	@Override
	public int add(DepartmentEntity entity) throws Exception {
		Set<Integer> ks = dao.checkDepartment().keySet();
		String str=null;
		for(Integer s:ks){
			str=dao.checkDepartment().get(s);
			if(s==entity.getDepartmentCode()){
				return -1;
			}
			if(str.equals((entity.getDepartmentName()))){
				return -2;
			}
		}
		return dao.add(entity);
	}
	
	@Override
	public int delete(Integer departmentCode) {
		return dao.delete(departmentCode);
	}

}
