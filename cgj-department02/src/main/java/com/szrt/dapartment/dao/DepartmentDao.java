package com.szrt.dapartment.dao;

import java.util.Map;

import com.szrt.dapartment.enity.DepartmentEntity;

public interface DepartmentDao {
	/**
	 * 添加
	 * @param entity
	 * @return
	 */
	int add(DepartmentEntity entity);
	
	/**
	 * 检查重名
	 * @param entity
	 * @return
	 */
	 Map<Integer, String> checkDepartment() throws Exception ;
	
	int delete(Integer code);
}
