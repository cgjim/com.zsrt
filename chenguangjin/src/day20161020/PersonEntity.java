package day20161020;

public class PersonEntity {
	private int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public PersonEntity(int age) {
		super();
		this.age = age;
	}
	
	public PersonEntity() {
		this.age = age;
	}
}
