package day20161020;

public class Wan {
	/**
	 * 门票价格为100
	 */
	int ticket=100;
	/**
	 * 根据年龄判断票的价格
	 * @param age
	 * @return
	 */
	public int money(int age){
		if(age>=18){
			ticket=ticket;
		}else if(age>=12){
			ticket=ticket/2;
		}else{
			ticket=0;
		}
		//最后才输出门票即可
		System.out.println("门票的价格为："+ticket);
		return ticket;
	}
}
