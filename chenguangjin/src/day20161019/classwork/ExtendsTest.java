package day20161019.classwork;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExtendsTest {
/**
 * 省长能做做么 
 */
	@Test
	public void test() {
		NomarchServiseImpl sheng=new NomarchServiseImpl();
		sheng.doit();
		
	}
/**市长做什么
 * 继续了省长的东西还写了自己的方法
 */
	@Test
	public void test1() {
		MayorEntity shi=new MayorEntity();
		shi.doit();
		shi.upMenting();
		shi.upshiMenting();
	}
	/**多重继承
	 *县长要做什么
	 */
	@Test
	public void test2() {
		MonitorEntity mo=new MonitorEntity();
		mo.doit();
		mo.upMenting();
		mo.upshiMenting();
		mo.upXianMenting();
		mo.tree();
	}
}
