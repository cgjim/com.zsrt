package day20161019.point07.test;

import org.junit.Test;

import day20161019.point07.impl.BaseServiceImpl;
import day20161019.point07.son.MySQLServiceImpl;

public class SimplTest {
/**
 * 一个继承了抽象类的类 
 */
	@Test
	public void test() {
		//可以用父类调用方法
		BaseServiceImpl mq=new MySQLServiceImpl();
		mq.getConnection();
		mq.add();
		mq.delete(1);
		mq.findById(1);
	}

}
