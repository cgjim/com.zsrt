package day20161019.point07.son;

import day20161019.point07.impl.BaseServiceImpl;

public class SqlServerServiceImpl extends BaseServiceImpl {
/**
 * 继承了抽象类 必须重写它的方法
 */
	@Override
	public String getConnection() {
		System.out.println("SqlServer连接成功！！");
		return null;
	}

}
