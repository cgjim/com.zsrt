package day20161019.point07.impl;

import day20161019.point07.service.BaseService;

public abstract class BaseServiceImpl implements BaseService {
	
	public abstract String getConnection();
	@Override
	public void add() {
		System.out.println("增加了一条信息");
		
	}
	@Override
	public void delete(int id) {
		System.out.println("删除了一条信息");
	}

	@Override
	public void update(int id) {
		System.out.println("更新了一条信息");
	}

	@Override
	public void findById(int id) {
		System.out.println("找到ID");
		
	}

}
