package day20161019.point07.service;

public interface BaseService {
	void add();
	void delete(int id);
	void update(int id);
	void findById(int id);
	
}
