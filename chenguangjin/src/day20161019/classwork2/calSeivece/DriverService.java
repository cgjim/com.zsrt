package day20161019.classwork2.calSeivece;
/**
 * 什么人是的士司机?   interface  描述的一类人的行为  DriverService
 * 1： 司机                                   会开车   drive()
	2： 开车的同时还有人给你钱      收钱      receiveMoney(int distance)  里程
 * @author Administrator
 *
 */
public interface DriverService {
	/**
	 * 会开车
	 * @return true 表示会开车   false 表示不会开车
	 */
	boolean drive();
	
	/**
	 * 根据里程数目得到打的费用
	 * @param distance
	 * @return 
	 */
	double receiveMoney(double distance);
	
}
