package day20161019.classwork2.impl;

import day20161019.classwork2.calSeivece.CalService;

public  class CalServiceImpl implements CalService{

	@Override
	public int add(int x,int y) {
		int sum=x+y;
		System.out.println(sum);
		return sum;
	}

	@Override
	public int sub(int x,int y) {
		int sum=x-y;
		System.out.println(sum);
		return sum;
	}

	@Override
	public int cal(int x,int y) {
		int sum=x*y;
		System.out.println(sum);
		return 0;
	}

	@Override
	public int chu(int x,int y) {
		int sum=x/y;
		System.out.println(sum);
		return 0;
	}

	@Override
	public int mod(int x,int y) {
		int sum=x%y;
		System.out.println(sum);
		return 0;
	}

}
