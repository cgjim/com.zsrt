package day20161019.classwork2;

import static org.junit.Assert.*;

import org.junit.Test;

import day20161019.classwork2.calSeivece.CalService;
import day20161019.classwork2.impl.CalServiceImpl;
import day20161019.classwork2.impl.PersonImpl;

public class InterfaceTest {

	@Test
	public void test() {
		CalService cs=new CalServiceImpl();
		cs.add(1, 1);
		cs.sub(2, 1);
		cs.cal(2, 2);
		cs.chu(2, 2);
		cs.mod(8, 2);
	}

	@Test
	public void test2() {
		PersonImpl cgj=new PersonImpl();
		double money=cgj.receiveMoney(11);
		System.out.println(money);
		cgj.drive();
		cgj.song();
	}
}
