package day20161019.abstracttest;

public interface StudentService {
	
	void listen();
	
	String writeHomeWork(String name);
	
	int printAge(int age);
}
