package day20161021;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.junit.Test;

public class SendMail {
	@Test
	public void sendMail() throws Exception {
		//建站服务器对象
		Properties prop = new Properties();
		// 你要发送 给谁的邮箱服务器 例如
		//搜狐：mail.sohu.com 、QQ:mail.qq.com、139：mail.10086.cn 163:mail.163.com
		prop.setProperty("mail.host", "mail.163.com");
		prop.setProperty("mail.transport.protocol", "smtp");
		prop.setProperty("mail.smtp.auth", "true");
		//prop.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		//prop.setProperty("mail.smtp.port", "465");
		//prop.setProperty("mail.smtp.socketFactory.port", "465");
		// 获取默认session对象
		// 使用JavaMail发送邮件的5个步骤
		// 1、创建session
		Session session = Session.getInstance(prop);
		// 开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
		session.setDebug(true);
		// 2、通过session得到transport对象
		Transport ts = session.getTransport();
		// 第三方登陆evjxinjsickcbcgb  zizuqgdifumkbebc
		//登陆邮箱
		ts.connect("mail.163.com", "15818245636", "jim12345");
		// 4、创建邮件
		Message message = createSimpleMail(session);
		// 5、发送邮件
		ts.sendMessage(message, message.getAllRecipients());
		ts.close();
	}

	public static MimeMessage createSimpleMail(Session session) throws Exception {
		// 创建邮件对象
		MimeMessage message = new MimeMessage(session);
		// 指明邮件的发件人
		message.setFrom(new InternetAddress("15818245636@163.com"));
		// 指明邮件的收件人
		message.setRecipient(Message.RecipientType.TO, new InternetAddress("593455003@qq.com"));
		// 邮件的标题
		message.setSubject("原来你是我最想遇到的人");
		// 邮件的文本内容
		message.setContent("....哈哈哈...", "text/html;charset=UTF-8");
		return message;
	}

}
