package pratice.day20161020;

import static org.junit.Assert.*;

import org.junit.Test;

public class AbstractTest {

	@Test
	public void test() {
		/**
		 * 同样的父类，执行同样的方法，结果不一样。
		 */
		Employee jl=new Manner();
		Employee sa=new Salaer();
		Employee ee=new EasyEmploee();
		jl.CalSalary();
		sa.CalSalary();
		ee.CalSalary();
		ee.eat();
	}

}
