package pratice.day20161021.test;

import static org.junit.Assert.*;

import java.util.Scanner;

import javax.swing.tree.ExpandVetoException;

import org.junit.Test;

public class ExceptionTest {
/**
 *  Scanner  从键盘获得1个字符串      "123"  "abc"
	再调用  Integer.parseInt  把字符串转化成整形。  这个方法可能有异常
	用 2 种方法处理这个异常  
 */
	@Test
	public void test() {
		System.out.println("请输入字符串！！");
		//别人给你的数，最好你要有一个空的容器装
		String w=null;
		input(w);
		try {
			int a=Integer.parseInt(w);
			System.out.println(a);
		} catch (Exception e) {
			System.out.println("出现异常");
			System.out.println(e);
		}
	}
	@Test
	public void test02() throws Exception {
		System.out.println("请输入字符串！！");
		String a=null;
		input(a);
		int b=Integer.parseInt(a);
		System.out.println(b);
	}
	/**
	 * scanner 方法 可以让用户不停地输入一个字符串
	 * @param b
	 * @return
	 */
	public String input(String b){
		Scanner sc=new Scanner(System.in);
		String a=sc.next();
		return a;
	}
}
