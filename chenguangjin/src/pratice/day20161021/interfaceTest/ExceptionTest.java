package pratice.day20161021.interfaceTest;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExceptionTest {

	@Test
	public void test() {
		int x=10;
		int y=0;
		int c=0;
		try{
			c=x/y;
		}catch(Exception ex){
			System.out.println("异常啦！！");
			System.out.println(ex);
		}finally{
			System.out.println("666");
			
		}
	}

	/**
	 * 数组下标越界异常
	 */
	@Test
	public void test01() {
		int[] a={1,2,3,4};
		for(int i=0;i<=4;i++){
			try{
				System.out.println(a[5]);				
			}catch(Exception ex){
				System.out.println("数组下标越界！！");
				System.out.println(ex);
			}finally{
				System.out.println("总是执行");
			}
		}
	}
	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void test02() throws Exception {
		int a=Integer.parseInt("dds");
		System.out.println(a);
	}
}
