package com.zsrt.t03;

import java.util.Scanner;

public class T03 {
/***
 * 用switch语法写1个 goHome()方法
 */
	public static int  goHome(){
		System.out.println("请输入你的职位");
		Scanner sc=new Scanner(System.in);
		String elo=sc.next();
		switch(elo){
			case "老板":System.out.println("坐小车回家"); break;
			case "白领":System.out.println("坐高铁回家");break;
			case "普工":System.out.println("坐硬座回家");break;
			default: System.out.println("走路回家");
		}
		return 0;
	}

}
