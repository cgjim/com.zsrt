package com.zsrt.t04;

import java.util.Scanner;

public class T04 {
	/**
	 * 请使用3目运算符判断age>=18 打印成年人 否则打印 未成年人
	 * @return
	 */
	public static int  age(){
		System.out.println("请输入你的年龄");
		Scanner sc=new Scanner(System.in);
		int age=sc.nextInt();
		String adult="成年人";
		String unAdult="未成年人";
		String b;
		b=age>=18?adult:unAdult;
		System.out.println(b);
		return 0;
	}
}
