package com.zsrt.t07;


public class T07 {
	
	public static void pass(){
		int len=4;
		char[] arr= {'a','b','e','z'};
		//xH(arr);
		for(int i=0;i<len;i++){
			if(arr[i]=='z'){
				arr[i]='a';
			}else{
				arr[i]+=1;
			}
		}
		for(int j=len-1;j>=0;j--){
			System.out.println(arr[j]);
		}
	}
	
	public static void xH(char[] a){
		for(int i:a){
			System.out.println(i);
		}
	}
}
