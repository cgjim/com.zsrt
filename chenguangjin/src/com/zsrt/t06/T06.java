package com.zsrt.t06;

public class T06 {
	/**增加
	 * 请加1个 91
	 * @return
	 */
	public static int  add(){
		int[] num={1,2,3,4,5};
		int[] num2=new int[num.length+1];
		for(int i=0;i<num.length;i++){
			num2[i]=num[i];
		}
		num2[num2.length-1]=91;
		
		for(int j:num2){
			System.out.println(j);
		}
		
		return 0;
	}
	
	/**删除
	 * 请删除1个 4 发现下标！
	 * 用新数组代替旧的数组   
	 * 取得==4的下标  然后用循环赋值，让这下标之前的数组给新的数组，还有这下标之后的给新的数组
	 * @return
	 */
	public static void  del(){
		int[] num={1,2,3,4,5};
		int[] num2=new int[num.length-1];
		for(int i=0;i<num.length;i++){
			if(num[i]==4){
				System.out.println("发现目标！");
				//第二个for 用循环赋值给新的数组
			for(int j=0;j<num2.length;j++){
				//让这下标之前的数组给新的数组
				if(j<i){
					num2[j]=num[j];
					//else 就是>=目标下标 将旧的加1就可以跳过要删除的数组
				}else{
				//让这下标之后的数组给新的数组
					num2[i]=num[i+1];
				}
				}
			}/*else{
				System.out.println("没找到！！");
			}*/
			
		}
		for(int j:num2){
			System.out.println(j);
		}

	}
	
	/**
	 * 请把 5变成 95
	 */
	public static int  alt(){
		int[] num={1,2,3,4,5};
		for(int i=0;i<num.length;i++){
			if(num[i]==5){
				num[i]=95;
			}
		}
		xXH(num);
		return 0;
	}
	
	/**
	 * 查询 2是否存在
	 */
	public static void  sel(){
		int[] num={1,1,3,4,5};
		boolean flag=false;	
		for(int i=0;i<num.length;i++){
			if(num[i]==2){
				flag=true;
				//如果存在就结束循环
				break;
			}
		}
		//写在循环外不受影响
		if(flag){
			System.out.println("2存在");
			
		}else{
			System.out.println("不存在！");
		}
	}
	
	public static void xXH(int[] a){
		for(int j:a){
			System.out.println(j);
		}
	}
}
