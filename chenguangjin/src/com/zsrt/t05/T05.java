package com.zsrt.t05;

/***
 * 请用合适的方法实现 int  类型的数据加减乘除求模方法 
 * @author Administrator
 *
 */
public class T05 {
	public static int add(int x,int y){
		int sum=x+y;
		System.out.println(sum);
		return sum;
	}
	public static int sub(int x,int y){
		int sum=x-y;
		System.out.println(sum);
		return sum;
	}
	public static int mul(int x,int y){
		int sum=x*y;
		System.out.println(sum);
		return sum;
	}
	public static int chu(int x,int y){
		int sum=x/y;
		System.out.println(sum);
		return sum;
	}
	public static int mod(int x,int y){
		int sum=x%y;
		System.out.println(sum);
		return sum;
		
	}
}
