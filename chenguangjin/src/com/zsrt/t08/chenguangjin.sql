/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.6.0-m4 : Database - 161017
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`161017` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `161017`;

/*Table structure for table `cgj_student` */

DROP TABLE IF EXISTS `cgj_student`;

CREATE TABLE `cgj_student` (
  `S_ID` int(11) NOT NULL AUTO_INCREMENT,
  `S_NAME` varchar(11) DEFAULT NULL,
  `S_BIRDAY` date DEFAULT NULL,
  `T_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`S_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `cgj_student` */

insert  into `cgj_student`(`S_ID`,`S_NAME`,`S_BIRDAY`,`T_ID`) values (1,'张仨','1993-07-19',1),(3,'王武','1993-07-19',1),(4,'陈刘','1994-07-26',1);

/*Table structure for table `cgj_teacher` */

DROP TABLE IF EXISTS `cgj_teacher`;

CREATE TABLE `cgj_teacher` (
  `T_ID` int(11) NOT NULL AUTO_INCREMENT,
  `T_NAME` varchar(11) DEFAULT NULL,
  `T_STARTTIME` date DEFAULT NULL,
  `T_GOWORLTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`T_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `cgj_teacher` */

insert  into `cgj_teacher`(`T_ID`,`T_NAME`,`T_STARTTIME`,`T_GOWORLTIME`) values (1,'李思文','2012-10-10','2016-10-17 09:08:21'),(2,'李明','2012-06-10','2016-10-17 09:08:21'),(3,'刘志军','2012-11-03','2016-10-17 09:08:21');

#删除学生表中id为2的学生信息
DELETE FROM cgj_student WHERE S_ID=2;

#根据ID修改名字和生日等  
UPDATE cgj_student 
set S_NAME='陈刘',S_BIRDAY='1994-07-26' 
WHERE S_ID=4;

#查找李思文老师带的学生信息

SELECT *
from cgj_student
where T_ID=(SELECT T_ID
from cgj_teacher
where T_NAME='李思文')

#根据id查找老师或者学生
SELECT t.T_NAME,s.*
from cgj_teacher t,cgj_student s
WHERE s.S_ID=t.T_ID
and t.T_ID=1


